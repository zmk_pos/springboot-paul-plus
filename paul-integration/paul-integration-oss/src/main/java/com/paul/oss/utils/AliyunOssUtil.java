package com.paul.oss.utils;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.paul.oss.config.AliYunOssConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.UUID;

@Service
public class AliyunOssUtil {

    @Autowired
    private AliYunOssConfig config;

    public String Upload(MultipartFile file, String randomName) {
        OSS ossClient = new OSSClientBuilder().build(config.getAliyunOssEndpoint(), config.getAliyunOssAccessKey(), config.getAliyunOssAccessSecret());
        try {
            InputStream inputStream = file.getInputStream();
            // 指定Content-Type
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(inputStream.available());
            metadata.setCacheControl("no-cache");
            metadata.setContentEncoding("utf-8");
            metadata.setContentType(file.getContentType());
            ossClient.putObject(config.getAliyunOssBucket(), randomName, inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
        }
        return "/" + randomName;
    }

    //阿里云OSS文件上传
    public String Upload(MultipartFile file, String suffix, String fileDir) {
        OSS ossClient = new OSSClientBuilder().build(config.getAliyunOssEndpoint(), config.getAliyunOssAccessKey(), config.getAliyunOssAccessSecret());
        String randomName = fileDir + UUID.randomUUID().toString() + System.currentTimeMillis() + suffix;
        try {
            InputStream inputStream = file.getInputStream();
            // 指定Content-Type
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(inputStream.available());
            metadata.setCacheControl("no-cache");
            metadata.setContentEncoding("utf-8");
            metadata.setContentType(file.getContentType());
            ossClient.putObject(config.getAliyunOssBucket(), randomName, file.getInputStream(), metadata);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
        }
        return "/" + randomName;
    }

    public String Upload(InputStream inputStream, String contentType, String fileName) {
        OSS ossClient = new OSSClientBuilder().build(config.getAliyunOssEndpoint(), config.getAliyunOssAccessKey(), config.getAliyunOssAccessSecret());
        try {
            // 指定Content-Type
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(inputStream.available());
            metadata.setCacheControl("no-cache");
            metadata.setContentEncoding("utf-8");
            metadata.setContentType(contentType);
            ossClient.putObject(config.getAliyunOssBucket(), fileName, inputStream, metadata);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
        }
        return "/" + fileName;
    }
}
