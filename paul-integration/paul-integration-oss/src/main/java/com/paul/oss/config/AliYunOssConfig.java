package com.paul.oss.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author ZMK
 * @date 2022/2/14 11:41
 * @description
 */
@Component
public class AliYunOssConfig {

    /**
     * 阿里云oss key
     */
    @Value("${aliyun.oss.access-key}")
    private String aliyunOssAccessKey;

    /**
     * 阿里云oss secret
     */
    @Value("${aliyun.oss.access-secret}")
    private String aliyunOssAccessSecret;
    /**
     * 阿里云oss bucket
     */
    @Value("${aliyun.oss.bucket}")
    private String aliyunOssBucket;

    /**
     * 阿里云oss endpoint
     */
    @Value("${aliyun.oss.endpoint}")
    private String aliyunOssEndpoint;


    public String getAliyunOssAccessKey() {
        return aliyunOssAccessKey;
    }

    public void setAliyunOssAccessKey(String aliyunOssAccessKey) {
        this.aliyunOssAccessKey = aliyunOssAccessKey;
    }

    public String getAliyunOssAccessSecret() {
        return aliyunOssAccessSecret;
    }

    public void setAliyunOssAccessSecret(String aliyunOssAccessSecret) {
        this.aliyunOssAccessSecret = aliyunOssAccessSecret;
    }

    public String getAliyunOssBucket() {
        return aliyunOssBucket;
    }

    public void setAliyunOssBucket(String aliyunOssBucket) {
        this.aliyunOssBucket = aliyunOssBucket;
    }

    public String getAliyunOssEndpoint() {
        return aliyunOssEndpoint;
    }

    public void setAliyunOssEndpoint(String aliyunOssEndpoint) {
        this.aliyunOssEndpoint = aliyunOssEndpoint;
    }

}
