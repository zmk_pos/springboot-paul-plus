package com.paul.oss.utils;

import com.paul.common.exception.BusinessException;
import com.paul.common.exception.ExceptionCode;
import com.paul.common.utils.DateUtil;
import com.paul.oss.config.FileConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static java.util.Arrays.asList;

@Service
public class FileUtil {

    @Autowired
    private FileConfig fileConfig;

    /**
     * 文件上传
     *
     * @param file
     * @param sType    小类型,1:文档.2:图片.3:音频.4:其他.5:压缩文件
     * @param fileSize 文件类型:多个,隔开并小写
     * @param fileType 文件大小：默认支持5M
     * @return
     */
    public String Upload(MultipartFile file, Integer sType, int fileSize, String fileType) {
        if (file == null) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "请上传文件");
        }
        String fileName = file.getOriginalFilename();
        if (StringUtils.isBlank(fileName)) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "文件名不能为空");
        }
        if (file.getSize() == 0) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "文件内容不能为空");
        }
        int maxSize = 1024 * 1024 * 5;
        if (fileSize > 0) {
            maxSize = 1024 * 1024 * fileSize;
        }
        if (fileSize > 50) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "限制大小50M内");
        }
        if (file.getSize() > maxSize) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "限制大小" + fileSize + "M内");
        }

        List<String> suffixList = new ArrayList<>();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
        switch (sType) {
            case 1:
                suffixList = asList("xls", "xlsx");
                break;
            case 2:
                suffixList = asList("jpg", "png", "jpeg");
                break;
            case 3:
                break;
            case 4:
                if (StringUtils.isBlank(fileType)) {
                    throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "文件类型必填");
                }
                String[] strs = fileType.split(",");
                for (String str : strs) {
                    suffixList.add(str);
                }
                break;
            case 5:
                suffixList = asList("zip", "rar", "doc", "docx");
                break;
        }
        if (!suffixList.contains(suffix)) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "文件格式错误");
        }

        String randomName = UUID.randomUUID().toString();
        String fileDir = "product";
        fileDir += DateUtil.getStringDate(new Date()).replace("-", "") + "/";

        String fileNameAndSuffix = randomName + "." + suffix;
        String folderPath = fileConfig.getProjectFilePath() + fileDir;
        String vPath = fileDir + fileNameAndSuffix;
        String fullPath = folderPath + fileNameAndSuffix;
        try {
            saveFile(fullPath, folderPath, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vPath;
    }

    /**
     * 保存文件
     *
     * @param fullPath   全路径
     * @param folderPath 文件夹路径
     * @param file
     * @throws IOException
     */
    public void saveFile(String fullPath, String folderPath, MultipartFile file) throws IOException {
        File fl = new File(folderPath);
        if (!fl.exists()) {
            fl.mkdirs();
        }
        InputStream is = file.getInputStream();
        FileOutputStream fos = new FileOutputStream(fullPath);
        byte[] b = new byte[1024];
        while ((is.read(b)) != -1) {
            fos.write(b);// 写入数据
        }
        is.close();
        fos.close();// 保存数据
    }

}



