package com.paul.oss.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author ZMK
 * @date 2022/2/14 11:51
 * @description
 */
@Configuration
public class FileConfig {

    //    @Value("${project.filepath}")
    private String ProjectFilePath;

    public String getProjectFilePath() {
        return ProjectFilePath;
    }

    public void setProjectFilePath(String projectFilePath) {
        ProjectFilePath = projectFilePath;
    }
}
