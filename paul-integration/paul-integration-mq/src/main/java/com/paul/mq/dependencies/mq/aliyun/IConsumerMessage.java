package com.paul.mq.dependencies.mq.aliyun;

import com.aliyun.openservices.ons.api.Action;

/**
 *  消息消费接口
 * Created by cyc_pipeline on 2019/9/26 17:01
 */
public interface IConsumerMessage {


    /**
     * 消息消费接口。
     * @param message
     * @return
     */
    Action onMessage(String message);
}
