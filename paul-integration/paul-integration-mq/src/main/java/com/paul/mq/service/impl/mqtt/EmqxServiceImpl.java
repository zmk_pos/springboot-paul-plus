package com.paul.mq.service.impl.mqtt;

import com.paul.mq.bean.vo.AliyunMQTTVO;
import com.paul.mq.bean.dto.AliMQTTDTO;
import com.paul.mq.bean.enums.MQTTEnum;
import com.paul.mq.service.IMQTTService;
import com.paul.mq.service.MQTTServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * @author ZMK
 * @date 2022/1/27 15:29
 * @description
 */
@Service
public class EmqxServiceImpl implements IMQTTService, InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(EmqxServiceImpl.class);


    @Override
    public String send() {
//        MqttConsumer.publish(topic, msg);
        log.info("EmqxServiceImpl=>send");
        return this.getType();
    }

    @Override
    public String getType() {
        return MQTTEnum.EMQX.toString();
    }

    @Override
    public AliyunMQTTVO SendMsg(AliMQTTDTO requestParameter) {
        return null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        MQTTServiceFactory.register(MQTTEnum.EMQX, this);
    }
}
