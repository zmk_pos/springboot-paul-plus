//package com.paul.mq.dependencies.mq.rabbitmq;
//
//import org.springframework.amqp.core.*;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.annotation.Resource;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Exchange和Queue配置
// *
// * @author zmk
// * @date 2021/1/12 12:02
// * @description
// */
//@Configuration
//public class QueueConfig {
//
//    @Resource
//    private RabbitTemplate rabbitTemplate;
//
//    @Bean
//    public CustomExchange delayExchange() {
//        Map<String, Object> args = new HashMap<>();
//        args.put("x-delayed-type", "direct");
//        return new CustomExchange("test_exchange", "x-delayed-message", true, false, args);
//    }
//
//    /**
//     * 定义队列
//     * Queue 可以有4个参数
//     * 1.队列名
//     * 2.durable       持久化消息队列 ,rabbitmq重启的时候不需要创建新的队列 默认true
//     * 3.auto-delete   表示消息队列没有在使用时将被自动删除 默认是false
//     * 4.exclusive     表示该消息队列是否只在当前connection生效,默认是false
//     */
//    @Bean
//    public Queue queue() {
//        Queue queue = new Queue("test_queue_1", true);
//        return queue;
//    }
//
//    @Bean
//    public Binding binding() {
//        return BindingBuilder.bind(queue()).to(delayExchange()).with("test_queue_1").noargs();
//    }
//}
