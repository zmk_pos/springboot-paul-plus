package com.paul.mq.bean.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by zmk523@163.com on 2019/10/9 12:56
 */
public class BusinessMQDTO extends BaseSmsDTO implements Serializable {

    @ApiModelProperty(value = "消息内容", required = true)
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    private String data;

}
