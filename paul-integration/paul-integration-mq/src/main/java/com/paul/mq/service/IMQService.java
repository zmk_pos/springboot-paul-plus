package com.paul.mq.service;

import com.paul.mq.bean.dto.AliMQDTO;
import com.paul.mq.bean.vo.AliyunMQVO;

/**
 * MQ接口
 * @author ZMK
 * @date 2022/1/27 15:19
 * @description
 */
public interface IMQService {

    String send();

    String getType();

    AliyunMQVO sendMsg(AliMQDTO aliMQDto);
}
