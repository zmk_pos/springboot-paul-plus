package com.paul.mq.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author ZMK
 * @date 2022/1/27 14:34
 * @description
 */
@Configuration
@ConfigurationProperties(prefix = "aliyun.mq")
public class AliyunMQConfig extends AliyunBaseConfig{

    @Value("${aliyun.mq.nameSrvAddr}")
    private String aliyunMQNameSrvAddr;
    @Value("${aliyun.mq.groupId}")
    private String aliyunMQGroupId;
    @Value("${aliyun.mq.topic}")
    private String aliyunMQTopic;

    public String getAliyunMQNameSrvAddr() {
        return aliyunMQNameSrvAddr;
    }

    public void setAliyunMQNameSrvAddr(String aliyunMQNameSrvAddr) {
        this.aliyunMQNameSrvAddr = aliyunMQNameSrvAddr;
    }

    public String getAliyunMQGroupId() {
        return aliyunMQGroupId;
    }

    public void setAliyunMQGroupId(String aliyunMQGroupId) {
        this.aliyunMQGroupId = aliyunMQGroupId;
    }

    public String getAliyunMQTopic() {
        return aliyunMQTopic;
    }

    public void setAliyunMQTopic(String aliyunMQTopic) {
        this.aliyunMQTopic = aliyunMQTopic;
    }
}
