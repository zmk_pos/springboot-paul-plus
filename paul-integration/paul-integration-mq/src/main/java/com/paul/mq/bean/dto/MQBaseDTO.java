package com.paul.mq.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by zmk523@163.com on 2019/10/10 9:49
 */
public class MQBaseDTO {
    @ApiModelProperty(value = "Topic", required = true)
    private String topic;
    @ApiModelProperty(value = "消息内容", required = true)
    private String body;


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
