package com.paul.mq.bean.vo;

/**
 * 登陆返回对象
 *
 * @author zmk
 * @date 2018/10/19 11:04
 */
public class LoginVO {
    private String token; // 令牌
    private long expire; // 令牌有效期
    private String key; // 加密密钥
    private String random; // 随机数，服务器生成
    private Long mid;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getRandom() {
        return random;
    }

    public void setRandom(String random) {
        this.random = random;
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }
}
