package com.paul.mq.service;

import com.paul.common.exception.BusinessException;
import com.paul.mq.bean.enums.MQEnum;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * MQ接口工厂
 *
 * @author ZMK
 * @date 2022/1/27 15:17
 * @description
 */
public class MQServiceFactory {
    private static final Map<MQEnum, IMQService> services = new ConcurrentHashMap<MQEnum, IMQService>();

    public static IMQService get(MQEnum mqEnum) {
        IMQService smsService = services.get(mqEnum);
        if (null == smsService) {
            throw new BusinessException(mqEnum + "服务不存在");
        }
        return smsService;
    }

    public static void register(MQEnum mqEnum, IMQService service) {
        services.put(mqEnum, service);
    }
}
