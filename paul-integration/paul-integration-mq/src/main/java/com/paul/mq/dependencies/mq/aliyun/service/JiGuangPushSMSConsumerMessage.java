package com.paul.mq.dependencies.mq.aliyun.service;//package com.ruoyi.mq.dependencies.mq.aliyun.service;
//
//import com.alibaba.fastjson.JSON;
//import com.aliyun.openservices.ons.api.Action;
//import com.paul.message.bean.dto.JPushDTO;
//import com.paul.message.bean.enums.JPushEnum;
//import com.paul.message.jiGuang.JPushBean;
//import com.paul.message.jiGuang.service.IJiGuangPushService;
//import com.ruoyi.mq.dependencies.mq.aliyun.IConsumerMessage;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * 处理短信消息 极光
// * Created by cyc_pipeline on 2019/9/26 17:25
// */
//@Component
//@Slf4j
//public class JiGuangPushSMSConsumerMessage implements IConsumerMessage {
//
//    @Autowired
//    private IJiGuangPushService jiGuangPushService;
//
//    @Override
//    public Action onMessage(String message) {
//        try {
//            Boolean jpushMsgResult = false;
//            JPushDTO jPushDto = JSON.parseObject(message, JPushDTO.class);
//            JPushBean jPushBean = new JPushBean(jPushDto.getTitle(), jPushDto.getContent());
//            if (!StringUtils.isEmpty(jPushDto.getRegId())) {
//                if (jPushDto.getType().equals(JPushEnum.ios)) {
//                    jpushMsgResult = jiGuangPushService.pushIos(jPushBean, jPushDto.getRegId());
//                } else {
//                    jpushMsgResult = jiGuangPushService.pushAndroid(jPushBean, jPushDto.getRegId());
//                }
//            } else {
//                if (jPushDto.getType().equals(JPushEnum.ios)) {
//                    jpushMsgResult = jiGuangPushService.pushIos(jPushBean);
//                } else if (jPushDto.getType().equals(JPushEnum.android)) {
//                    jpushMsgResult = jiGuangPushService.pushAndroid(jPushBean);
//                } else {
//                    jpushMsgResult = jiGuangPushService.pushAll(jPushBean);
//                }
//            }
//
//            log.info("aliyun sms template msg：data ={};result = {}", message, jpushMsgResult);
//            //确认消息已经消费
//            return Action.CommitMessage;
//        } catch (Exception ex) {
//            log.info("JiGuangPushSMSConsumerMessage", ex);
//            return Action.ReconsumeLater;
//        }
//    }
//}
