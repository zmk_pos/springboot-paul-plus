//package com.paul.mq.dependencies.mq.rabbitmq;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.rabbit.connection.CorrelationData;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.stereotype.Component;
//
///**
// * @author zmk
// * @date 2021/1/12 16:28
// * @description
// */
//
//@Component
//public class ConfirmCallbackService implements RabbitTemplate.ConfirmCallback {
//
//    private static final Logger log = LoggerFactory.getLogger(ConfirmCallbackService.class);
//
//    @Override
//    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
//
//        if (!ack) {
//            log.error("消息发送异常!");
//        } else {
//            log.info("confirmMessage ===> correlationData={} ,ack={}, cause={}", correlationData.getId(), ack, cause);
//        }
//    }
//
//}
