package com.paul.mq.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author ZMK
 * @date 2022/1/27 14:34
 * @description
 */
@Configuration
@ConfigurationProperties(prefix = "aliyun.mqtt")
public class AliyunMQTTConfig extends AliyunBaseConfig{

    @Value("${aliyun.mqtt.topic}")
    private String aliyunMQTTTopic;
    @Value("${aliyun.mqtt.groupId}")
    private String aliyunMQTTGroupId;
    @Value("${aliyun.mqtt.cleanSession}")
    private Boolean aliyunMQTTCleanSession;
    @Value("${aliyun.mqtt.endPoint}")
    private String aliyunMQTTEndPoint;
    @Value("${aliyun.mqtt.instanceId}")
    private String aliyunMQTTInstanceId;
    @Value("${aliyun.mqtt.qosLevel}")
    private Integer aliyunMQTTQosLevel;
    @Value("${aliyun.mqtt.completionTimeout}")
    private Integer completionTimeout;
    @Value("${aliyun.mqtt.serviceTag}")
    private String aliyunMQTTServiceTag;

    public String getAliyunMQTTTopic() {
        return aliyunMQTTTopic;
    }

    public void setAliyunMQTTTopic(String aliyunMQTTTopic) {
        this.aliyunMQTTTopic = aliyunMQTTTopic;
    }

    public String getAliyunMQTTGroupId() {
        return aliyunMQTTGroupId;
    }

    public void setAliyunMQTTGroupId(String aliyunMQTTGroupId) {
        this.aliyunMQTTGroupId = aliyunMQTTGroupId;
    }

    public Boolean getAliyunMQTTCleanSession() {
        return aliyunMQTTCleanSession;
    }

    public void setAliyunMQTTCleanSession(Boolean aliyunMQTTCleanSession) {
        this.aliyunMQTTCleanSession = aliyunMQTTCleanSession;
    }

    public String getAliyunMQTTEndPoint() {
        return aliyunMQTTEndPoint;
    }

    public void setAliyunMQTTEndPoint(String aliyunMQTTEndPoint) {
        this.aliyunMQTTEndPoint = aliyunMQTTEndPoint;
    }

    public String getAliyunMQTTInstanceId() {
        return aliyunMQTTInstanceId;
    }

    public void setAliyunMQTTInstanceId(String aliyunMQTTInstanceId) {
        this.aliyunMQTTInstanceId = aliyunMQTTInstanceId;
    }

    public Integer getAliyunMQTTQosLevel() {
        return aliyunMQTTQosLevel;
    }

    public void setAliyunMQTTQosLevel(Integer aliyunMQTTQosLevel) {
        this.aliyunMQTTQosLevel = aliyunMQTTQosLevel;
    }

    public Integer getCompletionTimeout() {
        return completionTimeout;
    }

    public void setCompletionTimeout(Integer completionTimeout) {
        this.completionTimeout = completionTimeout;
    }

    public String getAliyunMQTTServiceTag() {
        return aliyunMQTTServiceTag;
    }

    public void setAliyunMQTTServiceTag(String aliyunMQTTServiceTag) {
        this.aliyunMQTTServiceTag = aliyunMQTTServiceTag;
    }
}
