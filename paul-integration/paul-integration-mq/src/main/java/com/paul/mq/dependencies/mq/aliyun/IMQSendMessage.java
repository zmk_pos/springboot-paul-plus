package com.paul.mq.dependencies.mq.aliyun;


import com.paul.mq.bean.dto.AliMQDTO;
import com.paul.mq.bean.vo.AliyunMQVO;

/**
 * Created by zmk523@163.com on 2019/6/29 14:51
 * 消息发送
 */
public interface IMQSendMessage {

    /**
     * 消息发送
     *
     */
    AliyunMQVO sendMsg(AliMQDTO aliMQDto);
}
