package com.paul.mq.bean.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by zmk523@163.com on 2019/9/2 13:53
 */
public class AliyunMQTTVO {
    @ApiModelProperty(value = "设备ID")
    private String clientId;
    @ApiModelProperty(value = "消息ID")
    private String msgId;

    public AliyunMQTTVO(String clientId, String msgId) {
        this.clientId = clientId;
        this.msgId = msgId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
}
