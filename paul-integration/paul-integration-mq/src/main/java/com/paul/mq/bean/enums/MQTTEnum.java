package com.paul.mq.bean.enums;

/**
 * @author ZMK
 * @date 2022/1/27 15:13
 * @description
 */
public enum MQTTEnum {

    ALIYUN_MQTT,
    EMQX;
}
