package com.paul.mq.bean.dto;


import com.paul.mq.bean.enums.PushWayEnum;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by zmk523@163.com on 2019/8/28 11:21
 */
public class AliMQDTO extends MQBaseDTO implements Serializable {
    @ApiModelProperty(value = "延迟时间[单位：s]", required = true)
    private Long delaySeconds;
    @ApiModelProperty(value = "推送类型", required = true)
    private PushWayEnum wayEnum;


    public Long getDelaySeconds() {
        return delaySeconds;
    }

    public void setDelaySeconds(Long delaySeconds) {
        this.delaySeconds = delaySeconds;
    }

    public PushWayEnum getWayEnum() {
        return wayEnum;
    }

    public void setWayEnum(PushWayEnum wayEnum) {
        this.wayEnum = wayEnum;
    }

}
