package com.paul.mq.service.impl.mq;

import com.paul.mq.bean.dto.AliMQDTO;
import com.paul.mq.bean.enums.MQEnum;
import com.paul.mq.bean.vo.AliyunMQVO;
import com.paul.mq.dependencies.mq.aliyun.RocketMQProducer;
import com.paul.mq.service.IMQService;
import com.paul.mq.service.MQServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ZMK
 * @date 2022/1/27 15:33
 * @description
 */
@Service("MQAliyunServiceImpl")
public class AliyunServiceImpl implements IMQService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(AliyunServiceImpl.class);

    @Autowired
    private RocketMQProducer mqProducerMessage;

    @Override
    public String send() {
        logger.info("AliyunServiceImpl=>send");
        return this.getType();
    }

    @Override
    public String getType() {
        return MQEnum.ALIYUN_MQ.toString();
    }

    @Override
    public AliyunMQVO sendMsg(AliMQDTO aliMQDto) {
        String msgId = mqProducerMessage.sendMessage(aliMQDto);

        AliyunMQVO aliyunMQVo = new AliyunMQVO();
        aliyunMQVo.setTopic(aliMQDto.getTopic());
        aliyunMQVo.setTag(aliMQDto.getWayEnum());
        aliyunMQVo.setDelaySeconds(aliMQDto.getDelaySeconds());
        aliyunMQVo.setMsgId(msgId);
        return aliyunMQVo;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        MQServiceFactory.register(MQEnum.ALIYUN_MQ, this);
    }
}
