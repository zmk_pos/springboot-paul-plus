package com.paul.mq.dependencies.mq.aliyun;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.paul.mq.dependencies.mq.aliyun.service.ConsumerMessageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by zmk523@163.com on 2019/6/29 11:06
 */

@Component
public class RocketMQConsumer implements MessageListener {

    private static final Logger log = LoggerFactory.getLogger(RocketMQConsumer.class);

    @Autowired
    private ConsumerMessageManager consumerMessageManager;


    @Override
    public Action consume(Message message, ConsumeContext context) {
        log.info("mq consumer main receive ：{}", message);
        try {
            return consumerMessageManager.doOnMessage(message.getTag(), new String(message.getBody(), "utf-8"));
        } catch (Exception ex) {
            log.info("处理数据异常，消息内容：{}", message);
            return Action.ReconsumeLater;
        }
    }
}
