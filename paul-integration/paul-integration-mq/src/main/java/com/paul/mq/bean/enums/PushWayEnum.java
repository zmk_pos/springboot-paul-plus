package com.paul.mq.bean.enums;


/**
 * Created by zmk523@163.com on 2019/7/1 8:54
 */
public enum PushWayEnum {
    //<editor-fold desc="发送短信消息">
    JiGuangSMS(100001, "JiGuangSMS"),
    WeChatSMS(110001, "WeChatSMS"),
    AliyunSMS(120002, "AliyunSMS"),

    //</editor-fold>

    //<editor-fold desc="发送MQ消息">

    OrderCancel(200000, "OrderCancel"),
    OrderRefund(200001, "OrderRefund"),
    OrderOverdue(200002, "OrderOverdue"),

    //</editor-fold>

    //<editor-fold desc="示例">

    sms(000000, "sms");

    //</editor-fold>
    int code;
    String tag;

    PushWayEnum(int code, String tag) {
        this.code = code;
        this.tag = tag;
    }

    public static String allTag() {
        StringBuilder builder = new StringBuilder();
        PushWayEnum[] values = PushWayEnum.values();
        for (int i = 0; i < values.length; i++) {
            PushWayEnum pushWayEnum = values[i];
            if (i != 0) {
                builder.append("||");
            }
            builder.append(pushWayEnum.tag);
        }
        return builder.toString();
    }
}