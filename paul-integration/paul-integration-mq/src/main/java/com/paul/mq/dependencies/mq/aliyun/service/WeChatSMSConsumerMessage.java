package com.paul.mq.dependencies.mq.aliyun.service;//package com.ruoyi.mq.dependencies.mq.aliyun.service;
//
//import com.alibaba.fastjson.JSON;
//import com.aliyun.openservices.ons.api.Action;
//import com.ruoyi.mq.dependencies.mq.aliyun.IConsumerMessage;
//import com.ruoyi.mq.bean.dto.WeChatSMSDTO;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * 处理短信消息 微信
// * Created by cyc_pipeline on 2019/9/26 17:17
// */
//@Component
//public class WeChatSMSConsumerMessage implements IConsumerMessage {
//
//    private static final Logger log = LoggerFactory.getLogger(WeChatSMSConsumerMessage.class);
//
//    @Autowired
//    private WeChatSMSService weChatSMSService;
//
//    @Override
//    public Action onMessage(String message) {
//        try {
//            WeChatSMSDTO weChatSMSDto = JSON.parseObject(message, WeChatSMSDTO.class);
//            String wechatMsgResult = weChatSMSService.send(weChatSMSDto);
//            log.info("wechat template msg：data ={},result = {}", message, wechatMsgResult);
//            //确认消息已经消费
//            return Action.CommitMessage;
//        } catch (Exception ex) {
//            log.info("WeChatSMSConsumerMessage", ex);
//            return Action.ReconsumeLater;
//        }
//    }
//}
