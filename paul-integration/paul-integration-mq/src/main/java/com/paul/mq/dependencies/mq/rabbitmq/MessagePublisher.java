//package com.paul.mq.dependencies.mq.rabbitmq;
//
//import org.springframework.amqp.core.MessageDeliveryMode;
//import org.springframework.amqp.rabbit.connection.CorrelationData;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.UUID;
//
//
///**
// * @author zmk
// * @date 2021/1/12 14:05
// * @description
// */
//@Service
//public class MessagePublisher {
//
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//    @Autowired
//    private ConfirmCallbackService confirmCallbackService;
//    @Autowired
//    private ReturnCallbackService returnCallbackService;
//
//    public void push(String queueName, String msg) {
//        /**
//         * 确保消息发送失败后可以重新返回到队列中
//         * 注意：yml需要配置 publisher-returns: true
//         */
//        rabbitTemplate.setMandatory(true);
//
//        /**
//         * 消费者确认收到消息后，手动ack回执回调处理
//         */
//        rabbitTemplate.setConfirmCallback(confirmCallbackService);
//
//        /**
//         * 消息投递到队列失败回调处理
//         */
//        rabbitTemplate.setReturnCallback(returnCallbackService);
//
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println("消息发送时间:" + sdf.format(new Date()));
//        rabbitTemplate.convertAndSend("test_exchange", queueName, msg, message -> {
//            message.getMessageProperties().setHeader("x-delay", 3000);
//            message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
//            return message;
//        }, new CorrelationData(UUID.randomUUID().toString()));
//    }
//
//}
