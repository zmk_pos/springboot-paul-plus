package com.paul.mq.bean.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by zmk523@163.com on 2019/8/31 16:19
 */
public class AliMQTTDTO extends MQBaseDTO implements Serializable {
    @ApiModelProperty(value = "设备序号", required = true)
    private String terminalNo;

    public String getTerminalNo() {
        return terminalNo;
    }

    public void setTerminalNo(String terminalNo) {
        this.terminalNo = terminalNo;
    }
}