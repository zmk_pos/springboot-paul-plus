//package com.paul.mq.dependencies.mq.rabbitmq;
//
//import com.rabbitmq.client.Channel;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * @author zmk
// * @date 2021/1/12 14:08
// * @description
// */
//@Component
//public class MessageConsumer {
//
//    private static final Logger log = LoggerFactory.getLogger(MessageConsumer.class);
//
//    @RabbitHandler
//    @RabbitListener(queues = "test_queue_1")
//    public void receive(Message message, Channel channel) throws IOException {
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            // 采用手动应答模式, 手动确认应答更为安全稳定
//            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
//            System.out.println("消息接收时间:" + sdf.format(new Date()));
//            System.out.println("接收到的消息:" + new String(message.getBody()));
//        } catch (Exception ex) {
//            if (message.getMessageProperties().getRedelivered()) {
//
//                log.error("消息已重复处理失败,拒绝再次接收...");
//
//                channel.basicReject(message.getMessageProperties().getDeliveryTag(), false); // 拒绝消息
//            } else {
//
//                log.error("消息即将再次返回队列处理...");
//
//                channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
//            }
//
//        }
//
//    }
//
//}
