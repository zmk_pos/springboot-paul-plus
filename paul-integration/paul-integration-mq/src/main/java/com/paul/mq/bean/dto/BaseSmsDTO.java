package com.paul.mq.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by zmk523@163.com on 2019/9/2 10:07
 */
public class BaseSmsDTO {
    @ApiModelProperty(value = "消息编号", required = true)
    private String msgId;

    @ApiModelProperty(value = "时间戳（精确到毫秒）", required = true)
    private Long timeStamp;

    @ApiModelProperty(value = "异步通知地址", required = true)
    private String notityUrl;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getNotityUrl() {
        return notityUrl;
    }

    public void setNotityUrl(String notityUrl) {
        this.notityUrl = notityUrl;
    }
}
