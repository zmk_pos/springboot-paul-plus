package com.paul.mq.service.impl.mq;

import com.paul.mq.bean.dto.AliMQDTO;
import com.paul.mq.bean.enums.MQEnum;
import com.paul.mq.bean.vo.AliyunMQVO;
//import com.paul.mq.dependencies.mq.rabbitmq.MessagePublisher;
import com.paul.mq.service.IMQService;
import com.paul.mq.service.MQServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * @author ZMK
 * @date 2022/1/27 15:37
 * @description
 */
@Service
public class RabbitmqServiceImpl implements IMQService, InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(RabbitmqServiceImpl.class);


//    @Autowired
//    private MessagePublisher messagePublisher;

    @Override
    public String send() {
        log.info("RabbitmqServiceImpl=>send");
        return this.getType();
    }

    @Override
    public String getType() {
        return MQEnum.RABBITMQ.toString();
    }

    @Override
    public AliyunMQVO sendMsg(AliMQDTO aliMQDto) {
        return null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        MQServiceFactory.register(MQEnum.RABBITMQ, this);
    }
}
