package com.paul.mq.service;

import com.paul.mq.bean.dto.AliMQTTDTO;
import com.paul.mq.bean.vo.AliyunMQTTVO;

/**
 * @author ZMK
 * @date 2022/1/27 15:24
 * @description
 */
public interface IMQTTService {

    String send();

    String getType();

    AliyunMQTTVO SendMsg(AliMQTTDTO aliMQTTDTO);
}
