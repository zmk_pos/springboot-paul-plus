package com.paul.mq.dependencies.mq.aliyun.service;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.Action;
import com.paul.mq.bean.dto.BusinessMQDTO;
import com.paul.mq.dependencies.mq.aliyun.IConsumerMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 处理订单倒计时完成消息
 * Created by zmk523@163.com on 2019/10/9 12:52
 */

@Component
public class OrderCancelConsumerMessage implements IConsumerMessage {

    private static final Logger log = LoggerFactory.getLogger(OrderCancelConsumerMessage.class);

    @Override
    public Action onMessage(String message) {
        try {
            BusinessMQDTO businessMQDto = JSON.parseObject(message, BusinessMQDTO.class);
            String jsonStr = HttpUtil.get(businessMQDto.getNotityUrl(), 15000);
            log.info("aliyun mq orderCancel msg：data ={};result = {}", message, jsonStr);
            return Action.CommitMessage;
        } catch (Exception ex) {
            log.info("OrderCancelConsumerMessage", ex);
            return Action.ReconsumeLater;
        }
    }
}
