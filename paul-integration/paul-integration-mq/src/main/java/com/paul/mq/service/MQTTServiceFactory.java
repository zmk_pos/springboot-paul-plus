package com.paul.mq.service;


import com.paul.common.exception.BusinessException;
import com.paul.mq.bean.enums.MQTTEnum;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ZMK
 * @date 2022/1/27 15:18
 * @description
 */
public class MQTTServiceFactory {

    private static final Map<MQTTEnum, IMQTTService> services = new ConcurrentHashMap<MQTTEnum, IMQTTService>();

    public static IMQTTService get(MQTTEnum mqttEnum) {
        IMQTTService service = services.get(mqttEnum);
        if (null == service) {
            throw new BusinessException(mqttEnum + "服务不存在");
        }
        return service;
    }

    public static void register(MQTTEnum mqttEnum, IMQTTService service) {
        services.put(mqttEnum, service);
    }
}
