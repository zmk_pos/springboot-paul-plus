package com.paul.mq.dependencies.mq.aliyun.service;

import com.aliyun.openservices.ons.api.Action;
import com.paul.mq.dependencies.mq.aliyun.IConsumerMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 指令出货处理业务
 * Created by cyc_pipeline on 2019/9/26 17:14
 */
@Component
public class ShipmentConsumerMessage implements IConsumerMessage {

    private static final Logger log = LoggerFactory.getLogger(ShipmentConsumerMessage.class);

    @Override
    public Action onMessage(String message) {
        log.info("receive mq body is：" + message);
        return Action.CommitMessage;
    }
}
