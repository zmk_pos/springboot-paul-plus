package com.paul.mq.dependencies.mqtt.aliyun;

import com.paul.mq.config.AliyunMQTTConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by zmk523@163.com on 2019/10/25 15:55
 */
@Component
public class ConsumerMsgManager {

    private static final Logger log = LoggerFactory.getLogger(ConsumerMsgManager.class);

    @Autowired
    private AliyunMQTTConfig aliyunConfig;

    public void doOnMessage(String message) {
//        MQResponseDTO mqResponse = JSON.parseObject(message, MQResponseDTO.class);
//        String resultStr = "";
//        switch (mqResponse.getType()) {
//            case 100100://远程关闭售货机
//
//            default:
//                throw new IllegalArgumentException("该类型的消息不存在，请注意查看，type=" + message);
//        }

        log.info("ConsumerMsgManager ->  doOnMessage: mqtt msg=" + message + "; business =" + "resultStr");
    }
}
