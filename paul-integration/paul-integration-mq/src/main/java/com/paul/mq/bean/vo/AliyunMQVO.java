package com.paul.mq.bean.vo;

import com.paul.mq.bean.enums.PushWayEnum;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by zmk523@163.com on 2019/9/2 13:53
 */
public class AliyunMQVO {

    @ApiModelProperty(value = "订阅topic")
    private String topic;
    @ApiModelProperty(value = "标签")
    private PushWayEnum tag;
    @ApiModelProperty(value = "延迟时间【单位：s】")
    private Long delaySeconds;
    @ApiModelProperty(value = "消息ID")
    private String msgId;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public PushWayEnum getTag() {
        return tag;
    }

    public void setTag(PushWayEnum tag) {
        this.tag = tag;
    }

    public Long getDelaySeconds() {
        return delaySeconds;
    }

    public void setDelaySeconds(Long delaySeconds) {
        this.delaySeconds = delaySeconds;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
}
