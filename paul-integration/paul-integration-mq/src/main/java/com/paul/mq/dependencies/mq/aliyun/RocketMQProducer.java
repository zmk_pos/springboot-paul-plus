package com.paul.mq.dependencies.mq.aliyun;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.SendResult;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.paul.mq.bean.dto.AliMQDTO;
import com.paul.mq.config.AliyunMQConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class RocketMQProducer {

    private static final Logger logger = LoggerFactory.getLogger(RocketMQProducer.class);

    @Autowired
    private AliyunMQConfig config;
    @Autowired
    private ProducerBean producerBean;

    /**
     * 发送消息
     */
    public String sendMessage(AliMQDTO aliMQDto) {
        String topic = StringUtils.isBlank(aliMQDto.getTopic()) ? config.getAliyunMQTopic() : aliMQDto.getTopic();
        // Message Tag 可理解为 Gmail 中的标签，对消息进行再归类，方便 Consumer 指定过滤条件在 MQ 服务器过滤 "orderRefund",
        String tag = aliMQDto.getWayEnum().toString();
        String body = aliMQDto.getBody();
        Long delaySeconds = aliMQDto.getDelaySeconds();

        try {
            /**
             * 生产者 Bean 配置在 producer.xml 中,可通过 ApplicationContext 获取或者直接注入到其他类(比如具体的
             * Controller)中.
             */
            Message msg = new Message( //
                    // Message 所属的 Topic
                    topic,
                    // Message Tag 可理解为 Gmail 中的标签，对消息进行再归类，方便 Consumer 指定过滤条件在 MQ 服务器过滤
                    // "orderRefund",
                    tag,
                    // Message Body 可以是任何二进制形式的数据， MQ 不做任何干预
                    // 需要 Producer 与 Consumer 协商好一致的序列化和反序列化方式
                    body.getBytes("utf-8"));
            // 设置代表消息的业务关键属性，请尽可能全局唯一
            // 以方便您在无法正常收到消息情况下，可通过 MQ 控制台查询消息并补发
            // 注意：不设置也不会影响消息正常收发
            // msg.setKey("orderRefundKey");
            // 发送消息，只要不抛异常就是成功
            delaySeconds = delaySeconds == null ? 0L : delaySeconds * 1000;
            msg.setStartDeliverTime(System.currentTimeMillis() + delaySeconds);
            SendResult sendResult = producerBean.send(msg);
            logger.info("mq msg result={}", sendResult);
            return sendResult.getMessageId();
        } catch (Exception e) {
            logger.error("RoctekMQ消息发送失败,消息內容={},异常轨迹如下", aliMQDto, e);
        }
        return "";

    }
}
