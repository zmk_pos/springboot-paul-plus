package com.paul.mq.bean.enums;

/**
 * Created by zmk523@163.com on 2019/9/5 16:16
 */
public enum JPushEnum {
    all,
    ios,
    android,
    winphone,
    android_ios,
    android_winphone,
    ios_winphone;
}
