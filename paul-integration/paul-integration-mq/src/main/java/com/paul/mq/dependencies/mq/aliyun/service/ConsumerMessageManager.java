package com.paul.mq.dependencies.mq.aliyun.service;

import com.aliyun.openservices.ons.api.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by zmk523@163.com on 2019/10/8 17:59
 */
@Component
public class ConsumerMessageManager {

    private static final Logger log = LoggerFactory.getLogger(ConsumerMessageManager.class);

    @Autowired
    private ShipmentConsumerMessage shipmentConsumerMessage;
//    @Autowired
//    private WeChatSMSConsumerMessage weChatSMSConsumerMessage;
//    @Autowired
//    private AliyunSMSConsumerMessage aliyunSMSConsumerMessage;
//    @Autowired
//    private JiGuangPushSMSConsumerMessage jiGuangPushSMSConsumerMessage;
    @Autowired
    private OrderCancelConsumerMessage orderCancelConsumerMessage;

    /**
     * 調用方法。
     *
     * @param tag
     * @return
     */
    public Action doOnMessage(String tag, String message) {


        if (tag == null) {
            log.warn("消息没有正确的消费，tag={},消息内容={}", tag, message);
            return Action.CommitMessage;
        }
        // tag 与 PushWayEnum 连用
        switch (tag) {
            case "sms":
                return shipmentConsumerMessage.onMessage(message);
//            case "JiGuangSMS":
//                return jiGuangPushSMSConsumerMessage.onMessage(message);
//            case "WeChatSMS":
//                return weChatSMSConsumerMessage.onMessage(message);
//            case "AliyunSMS":
//                return aliyunSMSConsumerMessage.onMessage(message);
            case "OrderCancel":
                return orderCancelConsumerMessage.onMessage(message);
            default:
                throw new IllegalArgumentException("该类型的消息投递者已经存在，请注意查看，type=" + tag);
        }
    }
}
