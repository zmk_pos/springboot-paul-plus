package com.paul.mq.dependencies.mq.aliyun;


import com.paul.mq.bean.dto.AliMQDTO;
import com.paul.mq.bean.vo.AliyunMQVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MQSendMessageImpl implements IMQSendMessage {

    @Autowired
    private RocketMQProducer mqProducerMessage;

    @Override
    public AliyunMQVO sendMsg(AliMQDTO aliMQDto) {
        String msgId = mqProducerMessage.sendMessage(aliMQDto);

        AliyunMQVO aliyunMQVo = new AliyunMQVO();
        aliyunMQVo.setTopic(aliMQDto.getTopic());
        aliyunMQVo.setTag(aliMQDto.getWayEnum());
        aliyunMQVo.setDelaySeconds(aliMQDto.getDelaySeconds());
        aliyunMQVo.setMsgId(msgId);
        return aliyunMQVo;
    }

    public static void main(String[] args) {
        String str = UUID.randomUUID().toString().replaceAll("-", "");
//        send(str, 300000L);
        System.out.println("send:" + str);
    }

}