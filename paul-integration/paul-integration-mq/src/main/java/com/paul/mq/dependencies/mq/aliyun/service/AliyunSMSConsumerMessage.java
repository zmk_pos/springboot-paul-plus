package com.paul.mq.dependencies.mq.aliyun.service;
//
//import cn.hutool.http.HttpUtil;
//import com.alibaba.fastjson.JSON;
//import com.aliyun.openservices.ons.api.Action;
//import com.ruoyi.mq.dependencies.mq.aliyun.IConsumerMessage;
//import com.ruoyi.mq.bean.dto.AliyunSMSDTO;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
///**
// * 处理短信消息 阿里云
// * Created by cyc_pipeline on 2019/9/26 17:22
// */
//@Component
//public class AliyunSMSConsumerMessage implements IConsumerMessage {
//
//    private static final Logger log = LoggerFactory.getLogger(AliyunSMSConsumerMessage.class);
//
//    @Autowired
//    private AliyunSMSService aliyunSMSService;
//
//    @Override
//    public Action onMessage(String message) {
//        try {
//            AliyunSMSDTO aliyunSMSDto = JSON.parseObject(message, AliyunSMSDTO.class);
//            String url = aliyunSMSDto.getNotityUrl();
//
//            SendSmsResponse aliMsgResult = aliyunSMSService.SendTemplateSMS(aliyunSMSDto);
//            log.info("aliyun sms template msg：data ={};result = {}", message, JSON.toJSONString(aliMsgResult));
//
//            if (null == aliMsgResult || !"OK".equals(aliMsgResult.getCode())) {
//                HttpUtil.get(url + "/" + aliyunSMSDto.getMsgId() + "/" + 300, 5000);
//                return Action.ReconsumeLater;
//            }
//
//            HttpUtil.get(url + "/" + aliyunSMSDto.getMsgId() + "/" + 200, 5000);
//            //确认消息已经消费
//            return Action.CommitMessage;
//        } catch (Exception ex) {
//            log.info("AliyunSMSConsumerMessage", ex);
//            return Action.CommitMessage;
//        }
//
//    }
//}
