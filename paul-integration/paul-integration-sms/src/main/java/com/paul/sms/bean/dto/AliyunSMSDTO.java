package com.paul.sms.bean.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by zmk523@163.com on 2019/9/2 11:30
 */
public class AliyunSMSDTO implements Serializable {
    @ApiModelProperty("手机号")
    private String telPhone;
    @ApiModelProperty("短信签名")
    private String signName;
    @ApiModelProperty("模板编码")
    private String smsTemplate;
    @ApiModelProperty("消息内容（示例：{\\\"code\\\":\\\"123456\\\"}）")
    private String content;

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getSmsTemplate() {
        return smsTemplate;
    }

    public void setSmsTemplate(String smsTemplate) {
        this.smsTemplate = smsTemplate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
