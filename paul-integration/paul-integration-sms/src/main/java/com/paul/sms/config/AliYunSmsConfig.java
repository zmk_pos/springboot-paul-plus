package com.paul.sms.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author ZMK
 * @date 2022/2/14 14:15
 * @description
 */
@Configuration
@ConfigurationProperties(prefix = "aliyun.sms")
public class AliYunSmsConfig {

    private String AliYunSmsAccessKey;
    private String AliYunSmsSecretKey;

    public String getAliYunSmsAccessKey() {
        return AliYunSmsAccessKey;
    }

    public void setAliYunSmsAccessKey(String aliYunSmsAccessKey) {
        AliYunSmsAccessKey = aliYunSmsAccessKey;
    }

    public String getAliYunSmsSecretKey() {
        return AliYunSmsSecretKey;
    }

    public void setAliYunSmsSecretKey(String aliYunSmsSecretKey) {
        AliYunSmsSecretKey = aliYunSmsSecretKey;
    }
}
