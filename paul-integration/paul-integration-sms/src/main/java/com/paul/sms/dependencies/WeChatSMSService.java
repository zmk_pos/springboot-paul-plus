package com.paul.sms.dependencies;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.paul.sms.bean.dto.WeChatSMSDTO;
import com.paul.sms.bean.vo.WeChatVO;
import org.springframework.stereotype.Service;

/**
 * Created by zmk523@163.com on 2019/8/31 18:03
 */
@Service
public class WeChatSMSService {
    //https://mp.weixin.qq.com/advanced/tmplmsg?action=faq&token=1809804762&lang=zh_CN

    private final String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";

    /**
     * @param weChatSMSDto
     * @return 成功返回：{"errcode":0,"errmsg":"ok","msgid":200228332}
     */
    public WeChatVO sendMsg(WeChatSMSDTO weChatSMSDto) {
        String jsonResult = send(weChatSMSDto);
        return JSON.parseObject(jsonResult, WeChatVO.class);
    }

    /**
     * @param weChatSMSDto
     * @return 成功返回：{"errcode":0,"errmsg":"ok","msgid":200228332}
     */
    public String send(WeChatSMSDTO weChatSMSDto) {
        return HttpUtil.post(url + weChatSMSDto.getToken(), weChatSMSDto.getData(), 10000);
    }

    private void GetToken() {
        String tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
        HttpUtil.get(tokenUrl, 5000);
    }
}
