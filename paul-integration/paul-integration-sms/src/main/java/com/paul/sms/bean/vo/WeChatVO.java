package com.paul.sms.bean.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by zmk523@163.com on 2019/9/2 10:41
 */
public class WeChatVO {
    @ApiModelProperty(value = "错误码")
    private Integer errcode;
    @ApiModelProperty(value = "错误消息")
    private String errmsg;
    @ApiModelProperty(value = "消息ID")
    private String msgid;

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    @Override
    public String toString() {
        return "WeChatResult{" +
                "errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                ", msgid='" + msgid + '\'' +
                '}';
    }
}
