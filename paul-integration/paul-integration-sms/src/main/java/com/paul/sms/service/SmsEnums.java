package com.paul.sms.service;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/17 18:02
 * @description：
 */
public enum SmsEnums {
    Aliyun,
    TencentCloud,
    BaiduCloud,
    JdCloud;
}
