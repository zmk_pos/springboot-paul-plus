package com.paul.sms.service.impl;

import com.paul.sms.bean.dto.AliyunSMSDTO;
import com.paul.sms.service.ISmsService;
import com.paul.sms.service.SmsEnums;
import com.paul.sms.service.SmsServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/18 9:21
 * @description：
 */
@Service
public class TxSmsServiceImpl implements ISmsService, InitializingBean {
    private static final Logger logger = LoggerFactory.getLogger(TxSmsServiceImpl.class);

    @Override
    public String send() {
        logger.info("TxSmsService=>send");
        return this.getType();
    }

    @Override
    public String getType() {
        return SmsEnums.TencentCloud.toString();
    }

    @Override
    public boolean SendTemplateSMS(AliyunSMSDTO aliyunSMSDto) {
        return false;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        SmsServiceFactory.register(SmsEnums.TencentCloud, this);
    }
}
