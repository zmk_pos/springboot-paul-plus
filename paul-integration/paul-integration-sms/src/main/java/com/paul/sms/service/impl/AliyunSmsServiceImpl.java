package com.paul.sms.service.impl;

import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.paul.sms.bean.dto.AliyunSMSDTO;
import com.paul.sms.dependencies.AliyunSMSService;
import com.paul.sms.service.ISmsService;
import com.paul.sms.service.SmsEnums;
import com.paul.sms.service.SmsServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/17 18:01
 * @description：
 */
@Service
public class AliyunSmsServiceImpl implements ISmsService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(AliyunSmsServiceImpl.class);

    @Autowired
    private AliyunSMSService aliyunSMSService;

    @Override
    public String send() {
        logger.info("AliyunSmsService=>send");
        return this.getType();
    }

    @Override
    public String getType() {
        return SmsEnums.Aliyun.toString();
    }

    @Override
    public boolean SendTemplateSMS(AliyunSMSDTO aliyunSMSDto) {
        SendSmsResponse sendSmsResponse = aliyunSMSService.SendTemplateSMS(aliyunSMSDto);
        if(null==sendSmsResponse){
            return false;
        }
        return sendSmsResponse.getCode().equals("10000");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        SmsServiceFactory.register(SmsEnums.Aliyun, this);
    }
}
