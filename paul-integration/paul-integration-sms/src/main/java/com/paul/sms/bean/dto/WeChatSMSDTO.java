package com.paul.sms.bean.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by zmk523@163.com on 2019/9/2 10:29
 */
public class WeChatSMSDTO implements Serializable {
    @ApiModelProperty("token(微信官方限制2个小时有效)")
    private String token;
    @ApiModelProperty("发送内容（参考微信模板消息,示例：{\\\"touser\\\":\\\"XX\\\",\\\"template_id\\\":\\\"XX\\\",\\\"topcolor\\\":\\\"#FF0000\\\",\\\"data\\\":{\\\"first\\\":{\\\"value\\\":\\\"订单到期提醒\\\",\\\"color\\\":\\\"#173177\\\"},\\\"keyword1\\\":{\\\"value\\\":\\\"数量、单价与金额不一致\\\",\\\"color\\\":\\\"#173177\\\"},\\\"keyword2\\\":{\\\"value\\\":\\\"2019-09-02 10:25:12\\\",\\\"color\\\":\\\"#173177\\\"},\\\"keyword3\\\":{\\\"value\\\":\\\"2019-09-02 10:25:12\\\",\\\"color\\\":\\\"#173177\\\"},\\\"remark\\\":{\\\"value\\\":\\\"XX\\\",\\\"color\\\":\\\"#173177\\\"}}}）")
    private String data;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
