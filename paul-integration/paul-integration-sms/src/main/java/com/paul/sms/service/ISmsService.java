package com.paul.sms.service;

import com.paul.sms.bean.dto.AliyunSMSDTO;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/17 18:00
 * @description：短信接口
 */
public interface ISmsService {

    String send();

    String getType();

    boolean SendTemplateSMS(AliyunSMSDTO aliyunSMSDto);
}
