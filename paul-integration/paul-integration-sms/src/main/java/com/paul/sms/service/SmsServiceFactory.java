package com.paul.sms.service;

import com.paul.common.exception.BusinessException;
import com.paul.common.exception.ExceptionCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/18 9:23
 * @description：短信接口工厂
 */
public class SmsServiceFactory {
    private static final Logger logger = LoggerFactory.getLogger(SmsServiceFactory.class);

    private static final Map<SmsEnums, ISmsService> services = new ConcurrentHashMap<SmsEnums, ISmsService>();

    public static ISmsService get(SmsEnums smsEnums) {
        ISmsService smsService = services.get(smsEnums);
        if (null == smsService) {
            throw new BusinessException(ExceptionCode.DATA_NOT_FOUND.value(), smsEnums + "短信服务不存在");
        }
        return smsService;
    }

    public static void register(SmsEnums smsEnums, ISmsService service) {
        services.put(smsEnums, service);
    }
}
