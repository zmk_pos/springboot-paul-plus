package com.paul.sms.dependencies;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.paul.sms.bean.dto.AliyunSMSDTO;
import com.paul.sms.config.AliYunSmsConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zmk523@163.com on 2019/8/31 17:41
 * https://api.aliyun.com/?spm=a2c4g.11186623.2.15.706e60e2MAnDww#/?product=Dysmsapi&api=SendBatchSms&params={}&tab=DEMO&lang=JAVA
 * https://help.aliyun.com/document_detail/101414.html?spm=a2c4g.11186623.6.616.61a860e2Z9mIpC
 */
@Service
public class AliyunSMSService {

    private static final Logger logger = LoggerFactory.getLogger(AliyunSMSService.class);

    // 产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    // 产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    @Autowired
    private AliYunSmsConfig smsConfig;

    /**
     * 发送短信
     * 参考：https://help.aliyun.com/document_detail/101414.html?spm=a2c4g.11186623.6.616.61a860e2Z9mIpC
     *
     * @return 发送成功：{"Message":"OK","RequestId":"2184201F-BFB3-446B-B1F2-C746B7BF0657","BizId":"197703245997295588^0","Code":"OK"}
     */
    public SendSmsResponse SendTemplateSMS(AliyunSMSDTO aliyunSMSDto) {
        try {
            // 可自助调整超时时间
            System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
            System.setProperty("sun.net.client.defaultReadTimeout", "10000");

            // 初始化acsClient,暂不支持region化
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", smsConfig.getAliYunSmsAccessKey(), smsConfig.getAliYunSmsAccessKey());
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);

            // 组装请求对象-具体描述见控制台-文档部分内容
            SendSmsRequest request = new SendSmsRequest();
            // 必填:待发送手机号
            request.setPhoneNumbers(aliyunSMSDto.getTelPhone());
            // 必填:短信签名-可在短信控制台中找到
            request.setSignName(aliyunSMSDto.getSignName());
            // 必填:短信模板-可在短信控制台中找到
            request.setTemplateCode(aliyunSMSDto.getSmsTemplate());
            // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            // request.setTemplateParam("{\"name\":\"Tom\", \"code\":\"123\"}");
            if (!StringUtils.isBlank(aliyunSMSDto.getContent())) {
                request.setTemplateParam(aliyunSMSDto.getContent());
            }

            // 选填-上行短信扩展码(无特殊需求用户请忽略此字段)
            // request.setSmsUpExtendCode("90997");

            // 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
            // request.setOutId("yourOutId");

            // hint 此处可能会抛出异常，注意catch
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            logger.info(JSON.toJSONString(sendSmsResponse));
            return sendSmsResponse;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    /**
     * 批量发送短信
     */
    public void SendBatchSms() {
    }

}
