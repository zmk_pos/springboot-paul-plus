package com.paul.base.config;

import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * 打印配置信息
 * @author ZMK
 * @date 2022/8/25 22:49
 * @description https://blog.csdn.net/qq_17522211/article/details/117255207
 */
public class PrintConfig {

    private static final List<String> UN_PRINT = Arrays.asList("java.class.path", "config.md5Key", "config.rc4Key", "druid.password", "redis.password", "jasypt.encryptor.password");

    public static void printApplicationProperties(ConfigurableEnvironment environment) {
        StreamSupport.stream(
                environment.getPropertySources().spliterator(), false)
                .filter(propertySource -> (propertySource instanceof EnumerablePropertySource))
                .map(propertySource -> ((EnumerablePropertySource) propertySource).getPropertyNames())
                .flatMap(Arrays::stream).distinct().collect(Collectors.toMap(Function.identity(), environment::getProperty))
                .entrySet().stream().forEach(entry -> {
                    String key = entry.getKey();
                    if (!UN_PRINT.contains(key)) {
                        if(key.startsWith("custom.")) {
                            System.err.println(String.format("properties，[%s = %s] . ", key, entry.getValue()));
                        }
                    }
                }
        );
    }

}
