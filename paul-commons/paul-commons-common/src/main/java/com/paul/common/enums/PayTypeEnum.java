package com.paul.common.enums;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/9 14:35
 * @description：支付类型枚举
 */
public enum PayTypeEnum {

    //平台支付
    PT_BALANCE_PAY(0, "平台余额支付"),
    //支付宝
    ALIPAY_PAGE(10, "PC网页支付"),
    ALIPAY_WAP(11, "手机网页支付"),
    ALIPAY_SCAN(12, "扫码支付"),
    ALIPAY_APP(13, "APP支付"),
    ALIPAY_MINI_APP(14, "小程序支付"),
    ALIPAY_BAR_CODE(15, "条码/声波付"),
    ALIPAY_FACE_SCAN(16, "刷脸"),
    //微信
    WX_MWEB(20, "PC网页支付"),
    WX_WAP(21, "手机网页支付"),
    WX_SCAN(22, "扫码支付"),
    WX_APP(23, "APP支付"),
    WX_JSAPI(24, "小程序/微信公众号H5支付"),
    WX_BAR_CODE(25, "条码付"),
    WX_FACE_SCAN(26, "刷脸");


    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    PayTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
