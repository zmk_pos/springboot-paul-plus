package com.paul.common.enums;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/7 11:39
 * @description： 客户状态枚举
 */
public enum CustomerStatusEnum {
    NORMAL(0, "正常");


    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    CustomerStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
