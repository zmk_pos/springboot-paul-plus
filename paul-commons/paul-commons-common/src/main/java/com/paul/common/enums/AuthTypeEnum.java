package com.paul.common.enums;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/14 15:51
 * @description：授权类型
 */
public enum AuthTypeEnum {

    WECHAT(1,"微信授权"),
    ALIPAY(2,"支付宝授权");

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    AuthTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
