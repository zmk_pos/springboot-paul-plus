package com.paul.common.constant;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/12 14:11
 * @description：
 */
public class RedisKeyConst {
    public static final String CONFIG_KEY = "pual:";
    public static final String TOKEN_AUTH = CONFIG_KEY + "token:%s";
    /**
     * 请求ID
     */
    public static final String REQUEST_ID = "pt_request_id:";
    /**
     * 用户权限
     */
    public static final String USER_PERMI_SSION = "pt_user_permission:";


    /**
     *
     */
    public static final String ORDER_INFO_ID = "orders:info:%s";
}
