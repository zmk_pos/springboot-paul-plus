package com.paul.common.enums;
/**
 * @author ：zmk
 * @date ：Created in 2021/11/12 14:11
 * @description：
 */
public enum AuthEnum {
    Token,
    Bearer
}
