package com.paul.common.base;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 9:41
 * @description：
 */
public interface II18nService {
    String getMessage(String msgKey, Object[] args);

    String getMessage(String msgKey);
}
