package com.paul.common.utils;


import java.util.UUID;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/15 17:47
 * @description：
 */
public class StringUtil {

    public static String getGUIDStr() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static void main(String[] args) {
        System.out.println(getGUIDStr());
    }
}
