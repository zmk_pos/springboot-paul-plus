package com.paul.common.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.Locale;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 9:41
 * @description：
 */
@Service
public class I18nServiceImpl implements II18nService {

    @Autowired
    private MessageSource messageSource;

    public I18nServiceImpl(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public String getMessage(String msgKey, Object[] args) {
        return messageSource.getMessage(msgKey, args, LocaleContextHolder.getLocale());
    }

    @Override
    public String getMessage(String msgKey) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(msgKey, null, locale);
    }

    public static void main(String[] args) {
        Locale locale = LocaleContextHolder.getLocale();
        System.out.println(locale.toString());
    }




}
