package com.paul.common.exception;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 11:18
 * @description：
 */
public enum ExceptionCode {
    //正常
    ZNormal(0, ""),
    Normal(200, ""),
    UNAUTHORIZED(401, "未授权"),
    FORBIDDEN(403, "禁止访问"),

    REPEATED_REQUESTS(1001, "重复请求"),
    DATA_ERROR(2000, "业务数据错误"),
    DATA_NOT_FOUND(2002, "业务数据不存在"),
    DATA_FORMAT_ERROR(2003, "数据格式错误"),
    DATA_TYPE_ERROR(2004, "数据类型错误"),
    DATA_REPEAT(2005, " 数据重复"),
    DATA_UNSUPPORTED(2006, "数据没有授权"),
    DATA_INVALID(2007, "数据无效"),

    PARAMETER_ERROR(3000, "参数错误"),
    DATA_UNDELETE(4000, "有关联数据，不能删除该记录"),
    SERVICE_ERROR(9999, "系统异常，稍后再试"),

    NOT_BINDING(6000, "手机号未绑定"),
    ;


    private final int value;
    private final String describe;

    ExceptionCode(int value, String describe) {
        this.value = value;
        this.describe = describe;
    }

    public int value() {
        return this.value;
    }

    public String getDescribe() {
        return this.describe;
    }

}
