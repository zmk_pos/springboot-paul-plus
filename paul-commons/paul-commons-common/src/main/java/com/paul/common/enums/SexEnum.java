package com.paul.common.enums;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/7 11:33
 * @description： 性别枚举
 */
public enum SexEnum {
    MAN(1, "男"),
    WOMAN(0, "女"),
    UNKNOWN(2, "未知");

    private Integer code;
    private String desc;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    SexEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
