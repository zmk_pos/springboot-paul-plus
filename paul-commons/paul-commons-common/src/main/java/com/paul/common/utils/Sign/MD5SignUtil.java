package com.paul.common.utils.Sign;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;

/**
 * @author ZMK
 * @date 2022/12/19 14:13
 * @description
 */
public class MD5SignUtil {

    /**
     * 签名
     * @param text 内容
     * @param key 密钥
     * @param inputCharset 编码
     * @return
     */
    public static String sign(String text, String key, String inputCharset) {
        text = text + key;
        return DigestUtils.md5Hex(getContentBytes(text, inputCharset));
    }

    private static byte[] getContentBytes(String content, String charset) {
        if (charset == null || "".equals(charset)) {
            return content.getBytes();
        }
        try {
            return content.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名错误,指定的编码集不对,您目前指定的编码集是:" + charset);
        }
    }
}
