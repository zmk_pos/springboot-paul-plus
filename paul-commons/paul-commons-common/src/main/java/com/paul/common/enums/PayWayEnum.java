package com.paul.common.enums;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/9 15:04
 * @description：支付类型枚举
 */
public enum PayWayEnum {

    BALANCE_PAY(0, "余额支付"),
    ALI_PAY(1, "支付宝"),
    WX_PAY(2, "微信支付");

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    PayWayEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
