package com.paul.common.bean;

import com.paul.common.exception.ExceptionCode;

import java.io.Serializable;

public class ApiResult<T> implements Serializable {

    private int code;   //返回码 非0即失败
    private String msg; //消息提示
    private T data; //返回的数据

    public ApiResult() {
    }

    public ApiResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    public ApiResult(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public ApiResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ApiResult(ExceptionCode exceptionCode, T data) {
        this.code = exceptionCode.value();
        this.data = data;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public  ApiResult success(T data) {
        return new ApiResult(ExceptionCode.Normal, data);
    }

    public ApiResult fail(ExceptionCode exceptionCode, T data) {
        return new ApiResult(exceptionCode, data);
    }
}