package com.paul.common.utils;

import org.springframework.util.DigestUtils;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/12 14:11
 * @description：
 */
public class MD5Util {
    //盐，用于混交md5
    private static final String slat = "q6taw$@47@#%12";

    /**
     * 生成md5
     */
    public static String getMD5(String str) {
        String base = str + "/" + slat;
        return DigestUtils.md5DigestAsHex(base.getBytes());
    }

    public static String getMD5(String str, String slat1) {
        String base = str + "/" + slat1;
        return DigestUtils.md5DigestAsHex(base.getBytes());
    }
}
