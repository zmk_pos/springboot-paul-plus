package com.paul.common.enums;

/**
 * 云服务商
 * @author ：zmk
 * @date ：Created in 2022/5/17 10:16
 * @description：
 */
public enum CloudServiceEunm {
    /**
     * 七牛云
     */
    QINIU(1),
    /**
     * 阿里云
     */
    ALIYUN(2),
    /**
     * 腾讯云
     */
    QCLOUD(3);

    private int value;

    CloudServiceEunm(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
