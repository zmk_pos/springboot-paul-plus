package com.paul.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD) // 作用到方法上
@Retention(RetentionPolicy.RUNTIME) // 运行时有效
/**
 * @author ：zmk
 * @date ：Created in 2021/11/12 14:11
 * @description：防止重复提交标记注解
 */
public @interface NoRepeatSubmit {
    int value() default 2;
}
