package com.paul.common.constant;

public class MqttConst {
    /*
    设备信息同步回调地址
     */
    public static final String TERMINAL_CALL_BACK = "/jobterminal/update/status";

    /*
    商品信息同步回调地址
     */
    public static final String PRODUCT_CALL_BACK = "/jobproduct/update/status";

    /*
    广告推送回调地址
     */
    public static final String ADVERT_CALL_BACK = "/jobadvert/update/status";
}
