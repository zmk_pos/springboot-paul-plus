package com.paul.common.enums;

/**
 * @author ZMK
 * @date 2022/3/13 16:56
 * @description
 */
public enum ChannelTypeEnum {
    PT_APP(1, "平台APP"),
    WX_GZH(2, "微信公众号"),
    WX_MINI_APP(3, "微信小程序"),
    LOCAL_SCAN_WAP(4, "本地扫码H5"),
    LOCAL_SCAN_MINI_APP(5, "本地扫码小程序"),
    ALIPAY_SHH(6, "支付宝生活号"),
    ALIPAY_MINI_APP(7, "支付宝小程序");


    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    ChannelTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
