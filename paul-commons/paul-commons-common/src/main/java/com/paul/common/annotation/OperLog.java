package com.paul.common.annotation;

import java.lang.annotation.*;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/12 14:11
 * @description：操作日志注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperLog {
    /**
     * 模块说明
     *
     * @return
     */
    String operModul() default "";

    /**
     * 操作说明
     *
     * @return
     */
    String operDesc() default "";

    /**
     * 操作类型
     *
     * @return
     */
    String operType() default "0";

    /**
     * 操作等级
     *
     * @return
     */
    int operLevel() default 0;
}
