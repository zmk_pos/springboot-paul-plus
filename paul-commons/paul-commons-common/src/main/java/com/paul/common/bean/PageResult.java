package com.paul.common.bean;

import java.util.List;

/**
 * 分页统一格式返回
 * @param <T>
 */
public class PageResult<T> {

    public PageResult(long total, List<T> items){
        this.total=total;
        this.items=items;
    }

    private long total;
    private List<T> items;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
