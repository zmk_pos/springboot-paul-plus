package com.paul.common.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 10:43
 * @description：
 */
@Component
@Data
public class KeyConfig {


    //主键ID生成参数 workerId
    @Value("${snow.flake.worker.id}")
    private Integer snowFlakeWorkerId;

    //主键ID生成参数 dataCenterId
    @Value("${snow.flake.data.center.id}")
    private Integer snowFlakeDataCenterId;

}
