package com.paul.common.enums;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/10 15:46
 * @description：交易类型
 */
public enum TradeTypeEnum {

    ORDER_TRADE(1, "订单"),
    RECHARGE_TRADE(2, "充值");

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    TradeTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
