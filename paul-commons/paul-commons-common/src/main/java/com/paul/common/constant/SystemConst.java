package com.paul.common.constant;

/**
 * @author ZMK
 * @date 2022/2/13 17:01
 * @description
 */
public class SystemConst {
    public static final String ADMIN = "admin";
    public static final String DEFAULTPWD = "1234";
    public static final String BASE_PACKAGE = "com.paul";
    /**
     * 超管名称
     */
    public static final String ADMINNAME = "超级管理员";

    /**
     * 默认页码
     */
    public static final String PAGENUM = "1";

    /**
     * 每页大小
     */
    public static final String PAGESIZE = "15";

    /**
     * 默认时区
     */
    public static final String TIMEZONE = "Etc/GMT-8";

    /**
     * 默认币种
     */
    public static final String CURRENCY = "¥";

    /**
     * 默认国家编码 中国
     */
    public static final String COUNTRY = "10021";

    /*日期格式
     */
    public static final String DATEPATTERN = "yyyy-MM-dd";

    public static final String DATETIMEPATTERN = "yyyy-MM-dd HH:mm:ss";

}
