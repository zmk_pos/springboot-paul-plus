package com.paul.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author ：zmk
 * @date ：Created in 2022/5/6 17:07
 * @description：
 */
public class DateRangeUtil {

    /**
     * 计算两个时间中所有的月份
     *
     * @param date1 开始时间
     * @param date2 结束时间
     * @return
     * @throws ParseException
     */
    public static List<String> getMonths(String date1, String date2)  {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        // 声明保存日期集合
        List<String> list = new ArrayList<String>();
        try {
            // 转化成日期类型
            Date startDate = sdf.parse(date1);
            Date endDate = sdf.parse(date2);

            //用Calendar 进行日期比较判断
            Calendar calendar = Calendar.getInstance();
            while (startDate.getTime()<=endDate.getTime()){
                // 把日期添加到集合
                list.add(sdf.format(startDate));
                // 设置日期
                calendar.setTime(startDate);
                //把日期增加一天
                calendar.add(Calendar.MONTH, 1);
                // 获取增加后的日期
                startDate=calendar.getTime();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 计算两个时间内 所有的周数
     *
     * @param date1
     * @param date2
     * @return
     * @throws ParseException
     */
    public static List<String> getWeeks(String date1, String date2)  {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<String> dateList = new ArrayList<>();
        try {
            Date parse = sdf.parse(date1);
            Date parse2 = sdf.parse(date2);

            Calendar c1 = Calendar.getInstance();
            c1.setTime(parse);
            //转为周一
            int weekYear = c1.get(Calendar.YEAR);
            int weekOfYear = c1.get(Calendar.WEEK_OF_YEAR);
            c1.setWeekDate(weekYear, weekOfYear, Calendar.MONDAY);
            Calendar c2 = Calendar.getInstance();
            c2.setTime(parse2);
            int weekYear2 = c2.get(Calendar.YEAR);
            int weekOfYear2 = c2.get(Calendar.WEEK_OF_YEAR);
            c2.setWeekDate(weekYear2, weekOfYear2, Calendar.SUNDAY);
            while (true) {
                int weekNum = c1.get(Calendar.WEEK_OF_YEAR);
                dateList.add(c1.getWeekYear() + "-" + (weekNum > 9 ? weekNum : "0" + weekNum));
                if (c1.getTimeInMillis() >= c2.getTimeInMillis()) {
                    break;
                }
                //增加7天
                c1.setTimeInMillis(c1.getTimeInMillis() + 1000 * 60 * 60 * 24 * 7);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateList;
    }

    /**
     * 计算两个时间内所有日期
     *
     * @param date1
     * @param date2
     * @return
     * @throws ParseException
     */
    public static List<String> getDays(String date1, String date2)  {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<String> dateList = new ArrayList<String>();
        Long oneDay = 1000 * 60 * 60 * 24L;

        try {
            Long startTime = sdf.parse(date1).getTime();
            Long endTime = sdf.parse(date2).getTime();

            Long time = startTime;
            while (time < endTime) {
                Date d = new Date(time);
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String date = df.format(d);
                dateList.add(date);
                time += oneDay;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateList;

    }

    public static void main(String[] args) throws ParseException {
        List<String> days = getDays("2022-05-01", "2022-05-07");
        for (String str: days) {
            System.out.println(str);
        }

        List<String> months = getMonths("2022-01-01", "2022-05-07");
        for (String str: months) {
            System.out.println(str);
        }

        List<String> weeks = getWeeks("2022-01-01", "2022-05-07");
        for (String str: weeks) {
            System.out.println(str);
        }

    }
}
