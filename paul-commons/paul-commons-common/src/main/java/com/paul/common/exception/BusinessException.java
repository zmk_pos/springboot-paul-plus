package com.paul.common.exception;


/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 11:18
 * @description：
 */
public class BusinessException extends RuntimeException {
    private int code;   //返回码 非0即失败
    private String msg; //消息提示
    private Object data;

    public BusinessException(ExceptionCode ec) {
        this.code = ec.value();
        this.msg = ec.getDescribe();
    }

    public BusinessException(Integer code, String message) {
        this.code = code;
        this.msg = message;
    }

    public BusinessException(Integer code, String message, Exception ex) {
        this.code = code;
        this.msg = message;
    }

    public BusinessException(String message) {
        this.msg = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}