package com.paul.common.bean.dto;

import com.paul.common.enums.AuthTypeEnum;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/14 15:54
 * @description：
 */
public class AuthLoginDTO {

    private String appId;
    private String authCode;
    private AuthTypeEnum typeEnum;
    private String encrypData;
    private String sign;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public AuthTypeEnum getTypeEnum() {
        return typeEnum;
    }

    public void setTypeEnum(AuthTypeEnum typeEnum) {
        this.typeEnum = typeEnum;
    }

    public String getEncrypData() {
        return encrypData;
    }

    public void setEncrypData(String encrypData) {
        this.encrypData = encrypData;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
