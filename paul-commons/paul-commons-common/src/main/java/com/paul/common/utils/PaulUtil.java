package com.paul.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class PaulUtil {
    /**
     * 电话号码加密处理
     *
     * @param telphone
     * @return
     */
    public static String encryptionTelphone(String telphone) {
        if (StringUtils.isBlank(telphone)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(telphone.substring(0,3)).append("******").append(telphone.substring(9, 11));
        return sb.toString();
    }

    /**
     * 获取6位随机字符
     */
    public static String getRandom(Integer num) {
        Random random = new Random();
        String result = "";
        for (int i = 0; i < num; i++) {
            result += random.nextInt(10);
        }
        return result;
    }


    public static String getStr(Date date) {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(date);
    }

    public static String getDateStr(Date date) {
        return new SimpleDateFormat("yyyyMMddHHmm").format(date);
    }

    /**
     * 设备订单号：日期（精确到秒）+设备ID（截取：后6位）+随机4位
     */
    public static String getOrderNoByTerminal(Long terminalId) {
        String str = terminalId.toString();
        int tidLen = str.length();
        str = str.substring(tidLen - 7, tidLen);

        return getStr(new Date()) + str + getRandom(4);
    }

    /**
     * 充值订单号：日期（精确到秒）+客户ID（截取：后6位）+随机4位
     */
    public static String getOrderNoByCustomer(Long customerId) {

        String str = customerId.toString();
        int tidLen = str.length();
        str = str.substring(tidLen - 7, tidLen);

        return getStr(new Date()) + str + getRandom(4);
    }

    public static Date getNowDate() {
        return new Date();
    }

    public static String getUUIDStr() {
        return UUID.randomUUID().toString();

    }

    public static void main(String[] args) {
        System.out.println(getStr(new Date()));

        String str = "900000000000000370";
        int tidLen = str.length();
        str = str.substring(tidLen - 6, tidLen);
        System.out.println(str);
        System.out.println(getRandom(4));
    }
}
