//package com.paul.common.bean.bo;
//
//public class UserAuthPrincipal extends BaseUserAuthPrincipal {
//    private String userName;
//    private String roleName;
//    private Long tenantId;
//    private Long roleId;
//    private Boolean isAdmin;
//    private String timezone;
//    /*
//   登录类型 默认0 正常登录  1：模拟登录
//    */
//    private Integer loginType;
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getRoleName() {
//        return roleName;
//    }
//
//    public void setRoleName(String roleName) {
//        this.roleName = roleName;
//    }
//
//    public Long getTenantId() {
//        return tenantId;
//    }
//
//    public void setTenantId(Long tenantId) {
//        this.tenantId = tenantId;
//    }
//
//    public Long getRoleId() {
//        return roleId;
//    }
//
//    public void setRoleId(Long roleId) {
//        this.roleId = roleId;
//    }
//
//    public Boolean getIsAdmin() {
//        return isAdmin;
//    }
//
//    public void setIsAdmin(Boolean admin) {
//        isAdmin = admin;
//    }
//
//    public Boolean getAdmin() {
//        return isAdmin;
//    }
//
//    public void setAdmin(Boolean admin) {
//        isAdmin = admin;
//    }
//
//    public String getTimezone() {
//        return timezone;
//    }
//
//    public void setTimezone(String timezone) {
//        this.timezone = timezone;
//    }
//
//    public Integer getLoginType() {
//        return loginType;
//    }
//
//    public void setLoginType(Integer loginType) {
//        this.loginType = loginType;
//    }
//}
