package com.paul.common.enums;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/16 15:25
 * @description：支付状态
 */
public enum PayStatusEnum {

    NOT_PAY(0, "未支付"),
    STAY_PAY(1, "待支付"),
    HAVE_PAY(2, "已支付");

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    PayStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
