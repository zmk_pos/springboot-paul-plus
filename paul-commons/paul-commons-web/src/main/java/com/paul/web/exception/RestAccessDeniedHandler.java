package com.paul.web.exception;

import com.alibaba.fastjson.JSON;
import com.paul.common.base.II18nService;
import com.paul.common.bean.ApiResult;
import com.paul.common.exception.ExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/12 16:35
 * @description：当访问接口没有权限时，自定义的返回结果
 */
@Component
public class RestAccessDeniedHandler implements AccessDeniedHandler {
    @Autowired
    private II18nService ii18nService;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        String body = JSON.toJSONString(new ApiResult(ExceptionCode.FORBIDDEN.value(), ii18nService.getMessage("i18n.key.common.param.blockingAccess"), ""));
        ResponseHelper.print(response, body, ExceptionCode.FORBIDDEN.value());
    }
}
