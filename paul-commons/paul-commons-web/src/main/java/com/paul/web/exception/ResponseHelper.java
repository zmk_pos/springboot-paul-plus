package com.paul.web.exception;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ResponseHelper {
    public static void print(HttpServletResponse response, String result, int status){
        try {
            response.setStatus(200);
            response.setCharacterEncoding("UTF-8");//一般我们将字符编码设置成UTF-8.国际标准.
            response.setContentType("application/text;charset=utf-8");
            PrintWriter outer = response.getWriter();
            outer.print(result);
            outer.flush();
            outer.close();
        } catch (IOException e) {
            e.printStackTrace();
            response.setStatus(status);
        }
    }
}
