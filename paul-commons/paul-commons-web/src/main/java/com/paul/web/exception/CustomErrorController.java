package com.paul.web.exception;

import javax.servlet.http.HttpServletRequest;

import com.paul.common.bean.ApiResult;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author ：zmk
 * @date ：Created in 2021/11/12 14:11
 * @description：
 */
@RestController
public class CustomErrorController extends AbstractErrorController {

    public CustomErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @RequestMapping("/error")
    public ApiResult error(HttpServletRequest request, ErrorAttributeOptions options) {
        // 获取request中的异常信息，里面有好多，比如时间、路径啥的，大家可以自行遍历map查看
        //Map<String, Object> attributes = getErrorAttributes(request, options);
        // 这里只选择返回message字段
        return new ApiResult<>(getStatus(request).value(), "未授权", null);
    }

}
