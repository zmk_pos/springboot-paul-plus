package com.paul.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author ZMK
 * @date 2022/2/14 17:28
 * @description 全局配置类
 */
@Component
public class ComConfig {

    /**
     * OSS 文件地址
     */

    @Value("${local.project.url}")
    private String localProjectUrl;

}