package com.paul.log.aop;


import com.alibaba.fastjson.JSON;
import com.paul.common.utils.HttpContextUtil;
import com.paul.common.utils.IpUtil;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 10:34
 * @description：访问日志切面配置
 */
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Aspect
@Component
@Order(2)
public class WebLogAop {

    private final static Logger logger = LoggerFactory.getLogger(WebLogAop.class);

    private ThreadLocal<Long> startTime = new ThreadLocal<>();


    /**
     * 定义请求日志切入点，其切入点表达式有多种匹配方式,这里是指定路径
     */
    @Pointcut("execution(public * com.paul.*.api.controller.*.*(..))")
    public void WebLogAop() {
    }

    /**
     * 前置通知：
     * 1. 在执行目标方法之前执行，比如请求接口之前的登录验证;
     * 2. 在前置通知中设置请求日志信息，如开始时间，请求参数，注解内容等
     *
     * @param joinPoint
     * @throws Throwable
     */
    @Before("WebLogAop()")
    public void doBefore(JoinPoint joinPoint) {
        startTime.set(System.currentTimeMillis());
        // 接收到请求，记录请求内容
        HttpServletRequest request = HttpContextUtil.getRequest();
        //获取请求头中的User-Agent
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        // 记录下请求内容
        logger.info("请求开始时间：{}", LocalDateTime.now());
        logger.info("请求Token : {}", request.getHeader("Authorization"));
        logger.info("请求Url : {}", request.getRequestURL().toString());
        logger.info("请求方式 : {}", request.getMethod());
        logger.info("请求ip : {}", IpUtil.getIpAddr(request));
        logger.info("请求方法 : ", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
//        logger.info("请求参数 : {}", Arrays.toString(joinPoint.getArgs()));
        if (request.getMethod().equals(HttpMethod.POST.toString()) || request.getMethod().equals(HttpMethod.PUT.toString()) || request.getMethod().equals(HttpMethod.DELETE.toString())) {
            Object[] object = joinPoint.getArgs();
            if (!StringUtils.isBlank(request.getQueryString())) {
                logger.info("请求参数: " + request.getQueryString());
            }
            if (object.length > 0) {
                for (int i = 0; i < object.length; i++) {
                    Object obj = object[i];
                    //排除不必要参数
                    if (obj instanceof HttpServletRequest || obj instanceof HttpServletResponse || obj instanceof MultipartFile || obj instanceof MultipartFile[]) {
                        continue;
                    }
                    logger.info("请求参数: " + JSON.toJSONString(obj));
                }
            }
        }
        if (request.getMethod().equals(HttpMethod.GET.toString())) {
            logger.info("请求参数: " + request.getQueryString());
        }
        // 系统信息
        logger.info("浏览器：{}", userAgent.getBrowser().toString());
        logger.info("浏览器版本：{}", userAgent.getBrowserVersion());
        logger.info("操作系统: {}", userAgent.getOperatingSystem().toString());

    }

    /**
     * 返回通知：
     * 1. 在目标方法正常结束之后执行
     * 1. 在返回通知中补充请求日志信息，如返回时间，方法耗时，返回值，并且保存日志信息
     *
     * @param ret
     * @throws Throwable
     */
    @AfterReturning(returning = "ret", pointcut = "WebLogAop()")
    public void doAfterReturning(Object ret) {
        // 处理完请求，返回内容
//        logger.info("SPEND TIME : " + (System.currentTimeMillis() - startTime.get()));
        // 处理完请求，返回内容
//        logger.info("RESPONSE : " + new Gson().toJson(ret));

        logger.info("请求结束时间：{}", LocalDateTime.now());
        logger.info("请求耗时：{}", System.currentTimeMillis() - startTime.get());
        // 处理完请求，返回内容
        logger.info("请求返回 : {}", JSON.toJSONString(ret));
        startTime.remove();
    }

    /**
     * 异常通知：
     * 1. 在目标方法非正常结束，发生异常或者抛出异常时执行
     * 1. 在异常通知中设置异常信息，并将其保存
     *
     * @param throwable
     */
    @AfterThrowing(value = "WebLogAop()", throwing = "throwable")
    public void doAfterThrowing(Throwable throwable) {
        // 保存异常日志记录
        logger.error("发生异常时间：{}", LocalDateTime.now());
        logger.error("抛出异常：{}", throwable.getStackTrace());
    }
}
