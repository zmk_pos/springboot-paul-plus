package com.paul.dependencies;

import cn.hutool.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Map;

/**
 * @author: zmk
 * @create: 2020-09-15 10:55
 * @description 四五短网址生成
 **/
public class DwzService {
    public String dwzCreate(String url) {
        try {
            String result = HttpRequest.post("https://45dwz.cn/apicreate.php?token=86z00n8ttdvyMlyHifcL0j9vq0iMDBFz")
                    .body("url=" + url, "application/x-www-form-urlencoded")
                    .timeout(1000)
                    .execute()
                    .body();
            Map<String, Object> map = new Gson().fromJson(result, new TypeToken<Map<String, Object>>() {
            }.getType());
            return map.get("shortUrl").toString();
        } catch (Exception ex) {
            return "";
        }
    }
}
