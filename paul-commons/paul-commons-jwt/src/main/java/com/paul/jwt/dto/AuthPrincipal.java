package com.paul.jwt.dto;

import com.paul.common.enums.AuthTypeEnum;

import javax.xml.crypto.Data;

/**
 * @author ZMK
 * @date 2022/12/12 17:39
 * @description
 */
public class AuthPrincipal<T> {
    private Long UserId;
    private Integer SysAuthType;
    private T PayLoad;

    public Long getUserId() {
        return UserId;
    }

    public void setUserId(Long userId) {
        UserId = userId;
    }

    public Integer getSysAuthType() {
        return SysAuthType;
    }

    public void setSysAuthType(Integer sysAuthType) {
        SysAuthType = sysAuthType;
    }

    public T getPayLoad() {
        return PayLoad;
    }

    public void setPayLoad(T payLoad) {
        PayLoad = payLoad;
    }
}
