package com.paul.jwt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author ZMK
 * @date 2022/2/14 13:34
 * @description
 */
@Component
public class JwtConfig {
    /**
     * JWT有效时长 毫秒 pc
     */
    @Value("${jwt.expire.time}")
    private Long jwtExpireTime;

    /**
     * JWT有效时长 毫秒 h5
     */
    @Value("${jwt.expire.time.mobile}")
    private Long jwtExpireTimeMobile;

    /**
     * JWT密钥
     */
    @Value("${jwt.secret}")
    private String jwtSecret;

    /**
     * 刷新token时长
     */
    @Value("${jwt.refresh.token.expire.time}")
    private Long refreshTokenExpireTime;

    public Long getJwtExpireTime() {
        return jwtExpireTime;
    }

    public void setJwtExpireTime(Long jwtExpireTime) {
        this.jwtExpireTime = jwtExpireTime;
    }

    public Long getJwtExpireTimeMobile() {
        return jwtExpireTimeMobile;
    }

    public void setJwtExpireTimeMobile(Long jwtExpireTimeMobile) {
        this.jwtExpireTimeMobile = jwtExpireTimeMobile;
    }

    public String getJwtSecret() {
        return jwtSecret;
    }

    public void setJwtSecret(String jwtSecret) {
        this.jwtSecret = jwtSecret;
    }

    public Long getRefreshTokenExpireTime() {
        return refreshTokenExpireTime;
    }

    public void setRefreshTokenExpireTime(Long refreshTokenExpireTime) {
        this.refreshTokenExpireTime = refreshTokenExpireTime;
    }
}
