package com.paul.mproot.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author ZMK
 * @date 2022/12/20 15:49
 * @description
 */
public interface BaseService<T> extends IService<T> {

}
