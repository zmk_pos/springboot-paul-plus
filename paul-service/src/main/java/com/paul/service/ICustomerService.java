package com.paul.service;

import com.paul.common.bean.PageResult;
import com.paul.domain.dto.CustomerQueryDTO;
import com.paul.model.Customer;
import com.paul.mproot.service.BaseService;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author ZMK
 * @date 2022/12/20 12:13
 * @description
 */
public interface ICustomerService extends BaseService<Customer> {

    Long add(Customer customer);

    PageResult<Customer> queryAll(CustomerQueryDTO criteria, Pageable pageable);

    List<Customer> queryAll(CustomerQueryDTO criteria);
}
