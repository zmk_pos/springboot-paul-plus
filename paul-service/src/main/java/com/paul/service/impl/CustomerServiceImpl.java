package com.paul.service.impl;

import com.github.pagehelper.PageInfo;
import com.paul.common.bean.PageResult;
import com.paul.dao.CustomerMapper;
import com.paul.domain.dto.CustomerQueryDTO;
import com.paul.model.Customer;
import com.paul.mproot.service.impl.BaseServiceImpl;
import com.paul.mproot.utils.QueryHelpPlus;
import com.paul.service.ICustomerService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author ZMK
 * @date 2022/12/20 12:12
 * @description
 */
@Service
public class CustomerServiceImpl extends BaseServiceImpl<CustomerMapper, Customer> implements ICustomerService {

    @Autowired
    private CustomerMapper customerMapper;

    @Override
    public Long add(Customer customer) {
        customer.setStatus(0);
        customer.setSalt(RandomStringUtils.randomNumeric(8));
        customer.setAddTime(new Date());
        customerMapper.insert(customer);
        return customer.getId();
    }

    @Override
    public PageResult<Customer> queryAll(CustomerQueryDTO criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<Customer> pageInfo = new PageInfo<>(queryAll(criteria));
        return new PageResult<>(pageInfo.getTotal(), pageInfo.getList());
    }


    @Override
    public List<Customer> queryAll(CustomerQueryDTO criteria) {
        return customerMapper.selectList(QueryHelpPlus.getPredicate(Customer.class, criteria));
    }
}
