package com.paul.ps.api.controller;

import com.paul.common.annotation.NoRepeatSubmit;
import com.paul.common.exception.BusinessException;
import com.paul.common.exception.ExceptionCode;
import com.paul.oss.utils.AliyunOssUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;


/**
 * @author zmk
 * @version 1.0
 * @date 2020/8/8 16:44
 * @description
 */
@Api(description = "文件上传")
@RestController
@CrossOrigin
@RequestMapping("/upload")
public class UploadController {
    public static final Logger log = Logger.getLogger(UploadController.class);

    private final AliyunOssUtil aliyunOssUtil;
    private final ApplicationContext applicationContext;

    public UploadController(AliyunOssUtil aliyunOssUtil, ApplicationContext applicationContext) {
        this.aliyunOssUtil = aliyunOssUtil;
        this.applicationContext = applicationContext;
    }


    @NoRepeatSubmit
    @ApiOperation(value = "上传图片", notes = "支持格式：jpg|jpeg|png|gif|bmp    大小限制：10M")
    @PostMapping(value = "/image/{imgType}")
    public List<String> FileUpload(
            @ApiParam(value = "图片类型,1:故障.2:补货", required = true) @PathVariable Integer imgType,
            @ApiParam(value = "上传的文件", required = true) @RequestParam(value = "file", required = false) MultipartFile[] file) {
        if (file == null || file.length == 0) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "参数错误");
        }
        log.info("上传的参数信息" + Arrays.toString(file));
        if (file.length < 2 || file.length > 5) {
            throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "至少上传两张图片");
        }
        List<String> list = new ArrayList<>();
        for (MultipartFile item : file) {
            //String filePat = uploadService.getFilePath(aFile, imgType);
            String fileName = item.getOriginalFilename();
            if (StringUtils.isBlank(fileName) || item.getSize() == 0) {
                throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "上传的图片不能为空");
            }

            int maxSize = 1024 * 1024 * 10;
            if (item.getSize() > maxSize) {
                throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "上传的图片不能超过3M");
            }

            List<Integer> imgTypeList = asList(1, 2, 3, 4, 5);
            if (!imgTypeList.contains(imgType)) {
                throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "上传的图片的类型有误");
            }

            List<String> suffixList = asList("jpg", "jpeg", "png", "gif", "bmp");
            String suffix = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            if (!suffixList.contains(suffix)) {
                throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "上传的图片是不支持的格式");
            }
            String fileDir = "";
            switch (imgType) {
                case 1:
                    fileDir = "ps/fault/";
                    break;
                case 2:
                    fileDir = "ps/replenish/";
                    break;
                default:
                    throw new BusinessException(ExceptionCode.PARAMETER_ERROR.value(), "上传类型参数错误");

            }
            String filePat = aliyunOssUtil.Upload(item, suffix, fileDir);
            //String filePat = fileDir + UUID.randomUUID().toString() + System.currentTimeMillis() + suffix;
            //applicationContext.publishEvent(new FileUploadHandleEvent(item, filePat));
            list.add(filePat);
        }
        log.info("返回的上传信息" + list.toString());
        return list;


    }
}
