package com.paul.terminal.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.paul.*"})
@MapperScan(basePackages = {"com.paul.dao"})
public class PaulTerminalApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(PaulTerminalApplication.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PaulTerminalApplication.class);
	}
}