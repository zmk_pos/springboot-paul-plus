package com.paul.customer.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 10:14
 * @description：
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.paul.*"})
@MapperScan(basePackages = {"com.paul.dao","com.paul.customer.api.mapper"})
public class PaulCustomerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PaulCustomerApplication.class, args);
    }
}
