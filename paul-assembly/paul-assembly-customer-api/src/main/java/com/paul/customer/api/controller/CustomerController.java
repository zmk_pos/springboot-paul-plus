package com.paul.customer.api.controller;

import com.paul.common.annotation.IgnoreAuth;
import com.paul.common.bean.PageResult;
import com.paul.domain.convert.CustomerConvert;
import com.paul.domain.dto.CustomerDTO;
import com.paul.domain.dto.CustomerPageQueryDTO;
import com.paul.domain.dto.CustomerQueryDTO;
import com.paul.domain.vo.CustomerVO;
import com.paul.model.Customer;
import com.paul.service.ICustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author ZMK
 * @date 2022/12/20 12:21
 * @description
 */
@Tag(name = "客户端-APP - 客户信息")
@RestController
@RequestMapping(value = "/customer")
@Validated
public class CustomerController {

    @Autowired
    private ICustomerService customerService;


    @IgnoreAuth
    @Operation(summary = "获得客户信息")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @GetMapping("/get/{id}")
    public CustomerVO get(@PathVariable Long id) {
        Customer customer = customerService.getById(id);
        if (null == customer) {
            return null;
        }
        return CustomerConvert.INSTANCE.convert(customer);

    }

    @IgnoreAuth
    @Operation(summary = "新增客户信息")
    @PostMapping("/add")
    public Long add(@Valid @RequestBody CustomerDTO customerDTO) {
        return customerService.add(CustomerConvert.INSTANCE.convert(customerDTO));
    }


    @IgnoreAuth
    @Operation(summary = "获得客户信息列表")
    @GetMapping("/list")
    public List<CustomerVO> queryAll(CustomerQueryDTO criteria) {
        List<Customer> list = customerService.queryAll(criteria);
        return CustomerConvert.INSTANCE.convertList(list);
    }

    @IgnoreAuth
    @Operation(summary = "获得客户信息分页列表")
    @GetMapping("/page")
    public PageResult<CustomerVO> queryPageAll(CustomerPageQueryDTO criteria) {
        PageResult<Customer> pageResult = customerService.queryAll(criteria.getCriteria(), criteria.getPageable());
        return CustomerConvert.INSTANCE.convertPage(pageResult);
    }
}
