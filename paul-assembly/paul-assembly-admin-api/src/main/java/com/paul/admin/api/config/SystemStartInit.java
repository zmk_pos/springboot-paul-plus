package com.paul.admin.api.config;

import com.paul.base.config.PrintConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Component;

/**
 * 系统启动后初始化，此方法会在服务器启动后（已经开放端口），加载配置信息
 * @author ZMK
 * @date 2022/8/25 22:54
 * @description
 */
@Component
public class SystemStartInit implements CommandLineRunner {
    @Autowired
    private ConfigurableEnvironment environment;

    @Override
    public void run(String... args) throws Exception {
        PrintConfig.printApplicationProperties(environment);
    }

}
