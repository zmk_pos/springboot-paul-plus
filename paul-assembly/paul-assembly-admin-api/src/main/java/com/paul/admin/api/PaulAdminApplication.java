package com.paul.admin.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.paul.*"})
@MapperScan(basePackages = {"com.paul.dao"})
@SpringBootApplication
public class PaulAdminApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        try {
            SpringApplication.run(PaulAdminApplication.class, args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
