package com.paul.demo.api.controller;

import com.paul.common.annotation.IgnoreAuth;
import com.paul.common.bean.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;

/**
 * @author ZMK
 * @date 2022/12/12 20:34
 * @description
 */

@Api(description = "演示相关接口")
@CrossOrigin
@RestController
@RequestMapping("/demo")
public class DemoController {

    @IgnoreAuth
    @ApiOperation(value = "获取值", notes = "获取值", httpMethod = "GET")
    @GetMapping(value = "/getInt")
    public Integer getInt(@RequestParam("id") Integer id) {
        return id;
    }


    @IgnoreAuth
    @ApiOperation(value = "获取字符串", notes = "获取字符串", httpMethod = "GET")
    @GetMapping(value = "/getStr")
    public ApiResult<String> getInt(@RequestParam("name") String name) {
        return new ApiResult().success(name);
    }

    @IgnoreAuth
    @ApiOperation(value = "获取字符串", notes = "获取字符串", httpMethod = "POST")
    @PostMapping(value = "/testPost")
    public ApiResult<String> testPost(@RequestBody Object name) {
        return new ApiResult().success(name);
    }
}
