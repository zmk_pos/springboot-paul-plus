package com.paul.demo.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 10:14
 * @description：
 */
@SpringBootApplication
@EnableCaching
@ComponentScan(basePackages = {"com.paul.*"})
@MapperScan(basePackages = {"com.paul.dao"})
public class PaulDemoApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PaulDemoApplication.class, args);
    }
}
