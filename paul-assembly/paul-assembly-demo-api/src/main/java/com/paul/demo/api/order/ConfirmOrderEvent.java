package com.paul.demo.api.order;

import lombok.AllArgsConstructor;

/**
 * @author ：zmk
 * @date ：Created in 2021/11/11 10:14
 * @description：确认订单时的事件
 */
@AllArgsConstructor
public class ConfirmOrderEvent {

    /**
     *
     */
    private String orderNo;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
