# springboot主攻前后端快速开发脚手架

#### 介绍
springboot主攻后端快速开发脚手架,让开发人员只关注业务开发，同时降低中小企业多框架维护问题，同时支持jwt及token认证

### 版本归纳
##后端技术
SpringBoot 2.7.3
Swagger
redis
hutool
mybatis
mysql-connector
jwt
spring-security
lombok
##前端技术
node
yarn
webpack
eslint
@vue/cli ~3
ant-design-vue - Ant Design Of Vue
vue-cropper - Picture edit
@antv/g2 - AntV G2
Viser-vue - Antv/G2 of Vue


#### 项目结构


#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  接口签名
    GET请求
    ![img.png](file/get.png)
    POST请求（签名参数参考GET请求Header）
    ![img.png](file/post.png)
2.  对象拷贝工具
    apache < HuTool < Spring < cglib < Mapstruct
    (https://blog.csdn.net/weixin_42469135/article/details/124991532)
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
