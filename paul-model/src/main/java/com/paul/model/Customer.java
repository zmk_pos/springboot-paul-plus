package com.paul.model;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ZMK
 * @date 2022/12/20 11:49
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("customer")
@KeySequence("customer_seq")
public class Customer implements Serializable {

    /**
     * 主键
     */
    private Long id;
    private String salt;
    private String password;
    private String avatar;
    private String nickName;
    private String sex;
    private String telphone;
    private int status;
    private Date lastLoginTime;
    private Date addTime;
}
