package com.paul.domain.convert;

import com.paul.common.bean.PageResult;
import com.paul.domain.dto.CustomerDTO;
import com.paul.domain.vo.CustomerVO;
import com.paul.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author ZMK
 * @date 2023/6/18 9:53
 * @description
 */
@Mapper
public interface CustomerConvert {
    CustomerConvert INSTANCE = Mappers.getMapper(CustomerConvert.class);

    Customer convert(CustomerDTO bean);

    CustomerVO convert(Customer bean);

    List<CustomerVO> convertList(List<Customer> bean);

    PageResult<CustomerVO> convertPage(PageResult<Customer> bean);
}
