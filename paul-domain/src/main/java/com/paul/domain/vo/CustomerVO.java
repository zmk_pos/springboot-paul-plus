package com.paul.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * @author ZMK
 * @date 2022/12/20 12:24
 * @description
 */
@Schema(description = "客户信息")
@Data
public class CustomerVO {
    @Schema(description = "编号", required = true)
    private Long id;

    @Schema(description = "头像", required = true)
    private String avatar;

    @Schema(description = "昵称", required = true)
    private String nickName;

    @Schema(description = "性别", required = true)
    private String sex;

    @Schema(description = "手机号码", required = true)
    private String telphone;

    @Schema(description = "状态", required = true)
    private int status;

    @Schema(description = "最后一次登录时间", required = true)
    private Date lastLoginTime;

    @Schema(description = "添加时间", required = true)
    private Date addTime;
}
