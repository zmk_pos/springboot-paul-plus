package com.paul.domain.dto;


import com.paul.mproot.annotation.Query;
import lombok.Data;

import java.util.Set;

/**
 * @author ZMK
 * @date 2022/12/20 15:36
 * @description
 */
@Data
public class CustomerQueryDTO {

    @Query(type = Query.Type.IN, propName = "id")
    private Set<Long> ids;

    @Query(type = Query.Type.INNER_LIKE)
    private String nickName;
}
