package com.paul.domain.dto;

import lombok.Data;
import org.springframework.data.domain.Pageable;

/**
 * @author ZMK
 * @date 2022/12/20 17:44
 * @description
 */
@Data
public class CustomerPageQueryDTO {
    private CustomerQueryDTO criteria;
    private Pageable pageable;
}
