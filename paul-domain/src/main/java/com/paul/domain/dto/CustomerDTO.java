package com.paul.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author ZMK
 * @date 2022/12/20 12:24
 * @description
 */
@Schema(description = "客户信息录入")
@Data
public class CustomerDTO {
    @Schema(description = "头像地址", required = true,example = "http://xxx.png")
    @NotNull(message = "头像地址不能为空")
    private String avatar;

    @NotNull(message = "昵称不能为空")
    @Schema(description = "昵称", required = true,example = "李四")
    private String nickName;

    @NotNull(message = "性别不能为空")
    @Schema(description = "性别", required = true,example = "男")
    private String sex;

    @NotNull(message = "联系电话不能为空")
    @Schema(description = "联系电话", required = true,example = "18888888888")
    private String telphone;
}
