package com.paul.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.paul.model.Customer;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ZMK
 * @date 2022/12/20 11:57
 * @description
 */
@Mapper
public interface CustomerMapper extends BaseMapper<Customer> {
}
