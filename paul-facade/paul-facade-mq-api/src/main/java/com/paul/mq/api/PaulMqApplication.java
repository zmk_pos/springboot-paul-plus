package com.paul.mq.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/7 17:08
 * @description：
 */
@SpringBootApplication
public class PaulMqApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PaulMqApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PaulMqApplication.class);
    }
}
