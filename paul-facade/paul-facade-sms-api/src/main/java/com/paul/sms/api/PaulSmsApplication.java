package com.paul.sms.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author ：zmk
 * @date ：Created in 2022/3/7 17:09
 * @description：
 */
@SpringBootApplication
public class PaulSmsApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PaulSmsApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PaulSmsApplication.class);
    }
}

